package metrics

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

// Total number of calling authentication function
var AuthenticationCall prometheus.Counter
var AuthenticationSuccess prometheus.Counter
var AuthenticationFailed prometheus.Counter

// Total number of calling related to user
var PostUserHandlerCall prometheus.Counter
var GetUserHandlerCall prometheus.Counter
var GetUserPhoneHandlerCall prometheus.Counter
var DeleteUserPhoneHandlerCall prometheus.Counter
var PutUserPhoneHandlerCall prometheus.Counter

// Number of calling related to user but with errors
var PostUserHandlerErr prometheus.Counter
var GetUserHandlerErr prometheus.Counter
var GetUserPhoneHandlerErr prometheus.Counter
var DeleteUserPhoneHandlerErr prometheus.Counter
var PutUserPhoneHandlerErr prometheus.Counter

// Total number of calling related to newsletter
var PostNewsletterHandlerCall prometheus.Counter
var GetNewsletterHandlerCall prometheus.Counter
var GetNewsletterIDHandlerCall prometheus.Counter
var DeleteNewsletterIDHandlerCall prometheus.Counter
var PutNewsletterIDHandlerCall prometheus.Counter

// Number of calling related to newsletter but with errors
var PostNewsletterHandlerErr prometheus.Counter
var GetNewsletterHandlerErr prometheus.Counter
var GetNewsletterIDHandlerErr prometheus.Counter
var DeleteNewsletterIDHandlerErr prometheus.Counter
var PutNewsletterIDHandlerErr prometheus.Counter

// Total number of calling related to tags
var PostTagsHandlerCall prometheus.Counter
var GetTagsHandlerCall prometheus.Counter

// Number of calling related to tags but with errors
var PostTagsHandlerErr prometheus.Counter
var GetTagsHandlerErr prometheus.Counter

// Total number request to API
var TotalRequestsCall prometheus.Counter

var UserRequestDurationSecond *prometheus.HistogramVec
var NewsletterRequestDurationSecond *prometheus.HistogramVec

type MetricServer struct {
	Host      string
	Port      string
	Path      string
	MetricLog *logrus.Logger
}

func NewMetricServer() *MetricServer {
	return &MetricServer{}
}

func (m *MetricServer) Start(ctx context.Context) {
	srv := http.Server{Addr: fmt.Sprintf("%s:%s", m.Host, m.Port)}
	ml := m.MetricLog.WithFields(logrus.Fields{
		"metric":           "Notification Service metric server",
		"host:port params": fmt.Sprintf("%s:%s", m.Host, m.Port),
	})

	select {
	case <-ctx.Done():
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			ml.Fatalf("metric server error %v", err)
		}
		return
	default:
		//TotalRequests
		TotalRequestsCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "total_request_to_api",
			Help: "Total number of requests",
		})
		prometheus.Register(TotalRequestsCall)

		//Authentication Requests
		AuthenticationCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "authentication_call",
			Help: "Total number of calling authentication function",
		})
		AuthenticationSuccess = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "authentication_call_success",
			Help: "counter of successfully authenticated calls",
		})
		AuthenticationFailed = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "authentication_call_failed",
			Help: "counter of failed authenticated calls",
		})
		prometheus.Register(AuthenticationCall)
		prometheus.Register(AuthenticationSuccess)
		prometheus.Register(AuthenticationFailed)

		//User requests
		PostUserHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "create_user_call",
			Help: "request for creating a user",
		})
		GetUserHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_users_call",
			Help: "request for getting users",
		})
		GetUserPhoneHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_user_call",
			Help: "request for getting a user",
		})
		DeleteUserPhoneHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "delete_user_call",
			Help: "requesr for deleting a user",
		})
		PutUserPhoneHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "change_user_call",
			Help: "request for changing a user",
		})
		prometheus.Register(PostUserHandlerCall)
		prometheus.Register(GetUserHandlerCall)
		prometheus.Register(GetUserPhoneHandlerCall)
		prometheus.Register(DeleteUserPhoneHandlerCall)
		prometheus.Register(PutUserPhoneHandlerCall)

		PostUserHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "create_user_failed",
			Help: "failed request for creating a user",
		})
		GetUserHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_users_failed",
			Help: "failed request for getting users",
		})
		GetUserPhoneHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_user_failed",
			Help: "failed request for creating a user",
		})
		DeleteUserPhoneHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "delete_user_failed",
			Help: "failed request for deleting a user",
		})
		PutUserPhoneHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "changing_user_failed",
			Help: "failed request for changin a user",
		})

		prometheus.Register(PostUserHandlerErr)
		prometheus.Register(GetUserHandlerErr)
		prometheus.Register(GetUserPhoneHandlerErr)
		prometheus.Register(DeleteUserPhoneHandlerErr)
		prometheus.Register(PutUserPhoneHandlerErr)

		//Newsletter requests
		PostNewsletterHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "create_newsletter_call",
			Help: "request for creating a newsletter",
		})
		GetNewsletterHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_newsletters_call",
			Help: "request for getting newsletters",
		})
		GetNewsletterIDHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_newsletter_call",
			Help: "request for getting a newsletter",
		})
		DeleteNewsletterIDHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "delete_newsletter_call",
			Help: "requesr for deleting a newsletter",
		})
		PutNewsletterIDHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "change_newsletter_call",
			Help: "request for changing a newsletter",
		})

		prometheus.Register(PostNewsletterHandlerCall)
		prometheus.Register(GetNewsletterHandlerCall)
		prometheus.Register(GetNewsletterIDHandlerCall)
		prometheus.Register(DeleteNewsletterIDHandlerCall)
		prometheus.Register(PutNewsletterIDHandlerCall)

		PostNewsletterHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "create_newsletter_failed",
			Help: "failed request for creating a newsletter",
		})
		GetNewsletterHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_newsletters_failed",
			Help: "failed request for getting newsletters",
		})
		GetNewsletterIDHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_newsletter_failed",
			Help: "failed request for creating a newsletter",
		})
		DeleteNewsletterIDHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "delete_newsletter_failed",
			Help: "failed request for deleting a newsletter",
		})
		PutNewsletterIDHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "changing_newsletter_failed",
			Help: "failed request for changin a newsletter",
		})

		prometheus.Register(PostNewsletterHandlerErr)
		prometheus.Register(GetNewsletterHandlerErr)
		prometheus.Register(GetNewsletterIDHandlerErr)
		prometheus.Register(DeleteNewsletterIDHandlerErr)
		prometheus.Register(PutNewsletterIDHandlerErr)

		PostTagsHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "create_tag_call",
			Help: "request for creating a tag",
		})
		GetTagsHandlerCall = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_tags_call",
			Help: "request for getting tags",
		})
		prometheus.Register(PostTagsHandlerCall)
		prometheus.Register(GetTagsHandlerCall)

		PostTagsHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "create_tag_failed",
			Help: "failed request for creating a tag",
		})
		GetTagsHandlerErr = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "get_tags_failed",
			Help: "failed request for getting tags",
		})
		prometheus.Register(PostTagsHandlerErr)
		prometheus.Register(GetTagsHandlerErr)

		UserRequestDurationSecond = prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Namespace: "user",
			Name:      "request_duration_seconds",
			Help:      "Duration of the request",
			Buckets:   []float64{0.1, 0.15, 0.2, 0.25, 0.3},
		}, []string{"status", "method"})
		prometheus.MustRegister(UserRequestDurationSecond)

		NewsletterRequestDurationSecond = prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Namespace: "newsletter",
			Name:      "request_duration_seconds",
			Help:      "Duration of the request",
			Buckets:   []float64{0.1, 0.15, 0.2, 0.25, 0.3},
		}, []string{"status", "method"})
		prometheus.MustRegister(NewsletterRequestDurationSecond)

		http.Handle(fmt.Sprintf("/%s", m.Path), promhttp.Handler())

		go func() {
			m.MetricLog.Infof("Metric server is listening %s", fmt.Sprintf("http://%s:%s", m.Host, m.Port))
			err := srv.ListenAndServe()
			if err != nil && err != http.ErrServerClosed {
				ml.Fatalf("metric server error %v", err)
			}
			if err == http.ErrServerClosed {
				m.MetricLog.Info("graceful shutdown metric server")
				return
			}
		}()
	}
}
