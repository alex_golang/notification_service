// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"backend/models"
)

// GetStatisticsIDOKCode is the HTTP code returned for type GetStatisticsIDOK
const GetStatisticsIDOKCode int = 200

/*
GetStatisticsIDOK Ok

swagger:response getStatisticsIdOK
*/
type GetStatisticsIDOK struct {

	/*
	  In: Body
	*/
	Payload []*models.StatisticsID `json:"body,omitempty"`
}

// NewGetStatisticsIDOK creates GetStatisticsIDOK with default headers values
func NewGetStatisticsIDOK() *GetStatisticsIDOK {

	return &GetStatisticsIDOK{}
}

// WithPayload adds the payload to the get statistics Id o k response
func (o *GetStatisticsIDOK) WithPayload(payload []*models.StatisticsID) *GetStatisticsIDOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get statistics Id o k response
func (o *GetStatisticsIDOK) SetPayload(payload []*models.StatisticsID) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetStatisticsIDOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	payload := o.Payload
	if payload == nil {
		// return empty array
		payload = make([]*models.StatisticsID, 0, 50)
	}

	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

// GetStatisticsIDBadRequestCode is the HTTP code returned for type GetStatisticsIDBadRequest
const GetStatisticsIDBadRequestCode int = 400

/*
GetStatisticsIDBadRequest Bad request

swagger:response getStatisticsIdBadRequest
*/
type GetStatisticsIDBadRequest struct {
}

// NewGetStatisticsIDBadRequest creates GetStatisticsIDBadRequest with default headers values
func NewGetStatisticsIDBadRequest() *GetStatisticsIDBadRequest {

	return &GetStatisticsIDBadRequest{}
}

// WriteResponse to the client
func (o *GetStatisticsIDBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(400)
}

// GetStatisticsIDInternalServerErrorCode is the HTTP code returned for type GetStatisticsIDInternalServerError
const GetStatisticsIDInternalServerErrorCode int = 500

/*
GetStatisticsIDInternalServerError Internal Server Error

swagger:response getStatisticsIdInternalServerError
*/
type GetStatisticsIDInternalServerError struct {
}

// NewGetStatisticsIDInternalServerError creates GetStatisticsIDInternalServerError with default headers values
func NewGetStatisticsIDInternalServerError() *GetStatisticsIDInternalServerError {

	return &GetStatisticsIDInternalServerError{}
}

// WriteResponse to the client
func (o *GetStatisticsIDInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses

	rw.WriteHeader(500)
}
