// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// PutNewsletterIDHandlerFunc turns a function with the right signature into a put newsletter ID handler
type PutNewsletterIDHandlerFunc func(PutNewsletterIDParams, interface{}) middleware.Responder

// Handle executing the request and returning a response
func (fn PutNewsletterIDHandlerFunc) Handle(params PutNewsletterIDParams, principal interface{}) middleware.Responder {
	return fn(params, principal)
}

// PutNewsletterIDHandler interface for that can handle valid put newsletter ID params
type PutNewsletterIDHandler interface {
	Handle(PutNewsletterIDParams, interface{}) middleware.Responder
}

// NewPutNewsletterID creates a new http.Handler for the put newsletter ID operation
func NewPutNewsletterID(ctx *middleware.Context, handler PutNewsletterIDHandler) *PutNewsletterID {
	return &PutNewsletterID{Context: ctx, Handler: handler}
}

/*
	PutNewsletterID swagger:route PUT /newsletter/{id} putNewsletterId

Change/update newsletter
*/
type PutNewsletterID struct {
	Context *middleware.Context
	Handler PutNewsletterIDHandler
}

func (o *PutNewsletterID) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewPutNewsletterIDParams()
	uprinc, aCtx, err := o.Context.Authorize(r, route)
	if err != nil {
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}
	if aCtx != nil {
		*r = *aCtx
	}
	var principal interface{}
	if uprinc != nil {
		principal = uprinc.(interface{}) // this is really a interface{}, I promise
	}

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params, principal) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
