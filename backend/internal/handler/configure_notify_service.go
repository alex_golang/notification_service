// This file is safe to edit. Once it exists it will not be overwritten

package handler

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"

	"backend/internal/handler/operations"
	mymw "backend/internal/middleware"
	"backend/internal/serviceHanlders"
	servicehanlders "backend/internal/serviceHanlders"
)

//go:generate swagger generate server --target ../../../backend --name NotifyService --spec ../../api/api.yml --server-package internal/handler --principal interface{}

func configureFlags(api *operations.NotifyServiceAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.NotifyServiceAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	// api.BasicAuthAuth = serviceHanlders.Authentication
	api.BasicAuthAuth = serviceHanlders.AlwaysAuthenticated
	/*
		Newsletter responsible functions
	*/
	api.PostNewsletterHandler = operations.PostNewsletterHandlerFunc(servicehanlders.PostNewsletterHandler)
	api.GetNewsletterHandler = operations.GetNewsletterHandlerFunc(servicehanlders.GetNewsletterHandler)
	api.DeleteNewsletterIDHandler = operations.DeleteNewsletterIDHandlerFunc(servicehanlders.DeleteNewsletterIDHandler)
	api.GetNewsletterIDHandler = operations.GetNewsletterIDHandlerFunc(servicehanlders.GetNewsletterIDHandler)
	api.PutNewsletterIDHandler = operations.PutNewsletterIDHandlerFunc(servicehanlders.PutNewsletterIDHandler)

	/*
		User responsible functions
	*/

	api.DeleteUserPhoneHandler = operations.DeleteUserPhoneHandlerFunc(servicehanlders.DeleteUserPhoneHandler)
	api.GetUserHandler = operations.GetUserHandlerFunc(servicehanlders.GetUserHandler)
	api.PostUserHandler = operations.PostUserHandlerFunc(servicehanlders.PostUserHandler)
	api.GetUserPhoneHandler = operations.GetUserPhoneHandlerFunc(servicehanlders.GetUserPhoneHandler)
	api.PutUserPhoneHandler = operations.PutUserPhoneHandlerFunc(servicehanlders.PutUserPhoneHandler)

	/*
		Tags responsible functions
	*/
	api.GetTagsHandler = operations.GetTagsHandlerFunc(servicehanlders.GetTagsHandler)
	api.PostTagsHandler = operations.PostTagsHandlerFunc(servicehanlders.PostTagsHandler)

	/*
		Statistics responsible functions
	*/

	api.GetStatisticsHandler = operations.GetStatisticsHandlerFunc(servicehanlders.GetStatisticsHandler)
	api.GetStatisticsIDHandler = operations.GetStatisticsIDHandlerFunc(servicehanlders.GetStatisticsIDHandler)

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	// return handler
	return mymw.MetricTotalRequestsMiddleware(handler)
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	// return handler
	return mymw.ContextRequestMiddleware(handler)
}
