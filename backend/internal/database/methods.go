package psql

import (
	"backend/models"
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx"
	"github.com/lib/pq"
)

type Tags map[int64]string

var (
	EmptyInsert error = errors.New("no affected rows")
	EmptySelect error = errors.New("no select rows")
	TagError    error = errors.New("tag not exist")
)

func (i *Instance) AddClient(ctx context.Context, user models.User) error {

	tags, err := i.GetAllTags(ctx)
	if err != nil {
		return fmt.Errorf("Tags converting error %+v", err)
	}
	Tags := make([]int64, 0, len(user.Tags))
	var errTagsCount int
	for _, tagID := range user.Tags {
		if _, ok := tags[tagID]; ok {
			Tags = append(Tags, tagID)
		} else {
			errTagsCount++
		}
	}
	if errTagsCount > 0 {
		return TagError
	}

	_, err = i.Conn.Query(ctx, "INSERT INTO client(cell_phone, tags, timezone) VALUES($1,$2,$3);",
		user.Phone, Tags, user.Timezone)
	if err != nil {
		return fmt.Errorf("Insert error: %v", err)
	}
	return nil
}

func (i *Instance) GetAllTags(ctx context.Context) (Tags, error) {
	rows, err := i.Conn.Query(ctx, "SELECT tag_id, tag_name FROM tags;")
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}

	defer rows.Close()

	tags := make(Tags)
	var tagId int64
	var tagName string
	for rows.Next() {
		err := rows.Scan(&tagId, &tagName)
		if err != nil {
			return nil, fmt.Errorf("Select scan error: %v", err)
		}
		if _, ok := tags[tagId]; !ok {
			tags[tagId] = tagName // example: tags[1]="опилки"
		}
	}
	return tags, nil
}

func (i *Instance) GetClients(ctx context.Context) ([]*models.User, error) {
	rows, err := i.Conn.Query(ctx, "SELECT cell_phone,tags,timezone FROM client;")
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}

	defer rows.Close()

	clients := make([]*models.User, 0)

	for rows.Next() {
		client := new(models.User)
		err := rows.Scan(&client.Phone, pq.Array(&client.Tags), &client.Timezone)
		if err != nil {
			return nil, fmt.Errorf("Select scan error: %v", err)
		}
		clients = append(clients, client)
	}
	return clients, nil
}

func (i *Instance) GetClientByPhone(ctx context.Context, phone string) ([]*models.User, error) {
	rows, err := i.Conn.Query(ctx, "SELECT cell_phone,tags,timezone FROM client WHERE cell_phone=$1 LIMIT 1; ", phone)
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	defer rows.Close()

	clients := make([]*models.User, 0)

	for rows.Next() {
		client := new(models.User)
		err := rows.Scan(&client.Phone, pq.Array(&client.Tags), &client.Timezone)
		if err != nil {
			return nil, fmt.Errorf("Select scan error: %v", err)
		}
		clients = append(clients, client)
	}
	return clients, nil
}

func (i *Instance) DeleteClient(ctx context.Context, phone string) error {
	_, err := i.Conn.Query(ctx, "DELETE FROM client WHERE cell_phone=$1;", phone)
	if err != nil {
		return fmt.Errorf("Delete error: %v", err)
	}
	return nil
}
func (i *Instance) UpdateClient(ctx context.Context, user *models.User) error {

	tags, err := i.GetAllTags(ctx)
	if err != nil {
		return fmt.Errorf("Tags converting error %+v", err)
	}
	Tags := make([]int64, 0, len(user.Tags))
	var errTagsCount int
	for _, tag := range user.Tags {
		if _, ok := tags[tag]; ok {
			Tags = append(Tags, tag)
		} else {
			errTagsCount++
		}
	}
	if errTagsCount > 0 {
		return TagError
	}

	_, err = i.Conn.Query(ctx, "UPDATE client SET timezone=$2, tags=$3  WHERE cell_phone=$1;",
		user.Phone, user.Timezone, Tags)
	if err != nil {
		return fmt.Errorf("Update error: %v", err)
	}
	return nil
}

/*
NEWSLETTER
*/

func (i *Instance) AddNewsletter(ctx context.Context, newsletter models.Newsletter) error {
	_, err := i.Conn.Query(ctx, "INSERT INTO newsletter (start, stop, text, filter, filter_type) VALUES($1,$2,$3, $4, $5);",
		newsletter.StartDatetime, newsletter.StopDatetime, newsletter.Text, newsletter.Filter, newsletter.FilterType)
	if err != nil {
		return fmt.Errorf("Insert error: %v", err)
	}
	return nil
}

func (i *Instance) GetNewsletter(ctx context.Context) ([]*models.Newsletter, error) {
	rows, err := i.Conn.Query(ctx, "SELECT id,start,stop,text,filter_type,filter FROM newsletter;")
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select returned empty: %v", err)
	}
	defer rows.Close()

	newsletters := make([]*models.Newsletter, 0)

	for rows.Next() {
		news := new(models.Newsletter)
		err := rows.Scan(&news.ID, &news.StartDatetime, &news.StopDatetime, &news.Text, &news.FilterType, &news.Filter)
		if err != nil {
			return nil, fmt.Errorf("Select scan error: %v", err)
		}
		newsletters = append(newsletters, news)
	}
	return newsletters, nil
}

func (i *Instance) GetNewsletterByID(ctx context.Context, id int64) ([]*models.Newsletter, error) {
	rows, err := i.Conn.Query(ctx, "SELECT id,start,stop,text,filter,filter_type FROM newsletter WHERE id=$1;", id)
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select returned empty: %v", err)
	}
	defer rows.Close()

	newsletter := make([]*models.Newsletter, 0)

	for rows.Next() {
		news := new(models.Newsletter)
		err := rows.Scan(&news.ID, &news.StartDatetime, &news.StopDatetime, &news.Text, &news.Filter, &news.FilterType)
		if err != nil {
			return nil, fmt.Errorf("Select  newsletter id %d  scan error: %v", id, err)
		}
		newsletter = append(newsletter, news)
	}
	return newsletter, nil
}

func (i *Instance) DeleteNewsletter(ctx context.Context, id int64) error {
	_, err := i.Conn.Query(ctx, "DELETE FROM newsletter WHERE id=$1;", id)
	if err != nil {
		return fmt.Errorf("Delete newsletter id %d  error: %v", id, err)
	}
	return nil
}

func (i *Instance) UpdateNewsletter(ctx context.Context, news *models.Newsletter) error {
	_, err := i.Conn.Query(ctx, "UPDATE newsletter SET start=$2, stop=$3, text=$4, filter=$5, filter_type=$6   WHERE id=$1;",
		news.ID, news.StartDatetime, news.StopDatetime, news.Text, news.Filter, news.FilterType)
	if err != nil {
		return fmt.Errorf("Update newsletter id %d error: %v", news.ID, err)
	}
	return nil
}

func (i *Instance) AddTag(ctx context.Context, tagName string) error {
	_, err := i.Conn.Query(ctx, "INSERT INTO tags (tag_name) VALUES($1);", tagName)
	if err != nil {
		return fmt.Errorf("Insert error: %v", err)
	}
	return nil
}

func (i *Instance) GetTags(ctx context.Context) ([]*models.Tags, error) {
	rows, err := i.Conn.Query(ctx, "SELECT tag_id, tag_name FROM tags;")
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	defer rows.Close()

	tags := make([]*models.Tags, 0)
	var tagId int64
	var tagName *string
	for rows.Next() {
		err := rows.Scan(&tagId, &tagName)
		if err != nil {
			return nil, fmt.Errorf("Select scan error: %v", err)
		}
		tags = append(tags, &models.Tags{TagID: tagId, TagName: tagName})
	}
	return tags, nil
}

func (i *Instance) GetStatistic(ctx context.Context) (*models.Statistics, error) {
	ncount, err := i.Conn.Query(ctx, "SELECT COUNT(1) as ncount  FROM newsletter;")
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select newsletter returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	defer ncount.Close()

	mok, err := i.Conn.Query(ctx, "SELECT COUNT(1) as mok FROM message WHERE status='SENT';")
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select message-ok returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	defer mok.Close()

	mnok, err := i.Conn.Query(ctx, "SELECT COUNT(1) as mnok FROM message WHERE status='ERROR';")
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select message-nok returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	defer mnok.Close()

	payload := new(models.OverallStatistics)
	if ncount.Next() {
		err = ncount.Scan(&payload.NewsletterCount)
		if err != nil {
			return nil, fmt.Errorf("Select scan newsletter count error: %v", err)
		}
	}
	if mok.Next() {
		err = mok.Scan(&payload.OkMessageCount)
		if err != nil {
			return nil, fmt.Errorf("Select scan message-ok count error: %v", err)
		}
	}
	if mnok.Next() {
		err = mnok.Scan(&payload.NokMessageCount)
		if err != nil {
			return nil, fmt.Errorf("Select scan message-nok count error: %v", err)
		}
	}
	return &models.Statistics{Payload: payload}, nil
}

func (i *Instance) GetStatisticID(ctx context.Context, id int64) ([]*models.StatisticsID, error) {
	mok, err := i.Conn.Query(ctx, "SELECT COUNT(1) FROM message WHERE newsletter_id=$1 AND status='SENT';", id)
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select message-ok returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	defer mok.Close()

	mnok, err := i.Conn.Query(ctx, "SELECT COUNT(1) FROM message WHERE newsletter_id=$1 AND status='ERROR';", id)
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select message-nok returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}
	defer mnok.Close()

	stat := new(models.StatisticsPerID)
	stat.NewsletterID = &id
	if mok.Next() {
		err = mok.Scan(&stat.OkMessageCount)
		if err != nil {
			return nil, fmt.Errorf("Select scan message-ok count error: %v", err)
		}
	}
	if mnok.Next() {
		err = mnok.Scan(&stat.NokMessageCount)
		if err != nil {
			return nil, fmt.Errorf("Select scan message-nok count error: %v", err)
		}
	}
	stats := new(models.StatisticsID)
	stats.Payload = stat
	var statsOut []*models.StatisticsID
	statsOut = append(statsOut, stats)
	return statsOut, nil
}
