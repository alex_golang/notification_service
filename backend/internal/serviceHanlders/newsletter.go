package serviceHanlders

import (
	"context"
	"errors"
	"time"

	"backend/internal/handler/operations"
	"backend/internal/metrics"
	mymw "backend/internal/middleware"

	"github.com/go-openapi/runtime/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

/*
NEWSLETTER
*/

func PostNewsletterHandler(params operations.PostNewsletterParams, any interface{}) middleware.Responder {
	metrics.PostNewsletterHandlerCall.Inc()
	now := time.Now()

	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "PostNewsletterHandler",
		mymw.RequestContextID: reqID,
	})
	l.Debugf("input params: %+v", params.Newsletter.Payload)
	l.Info("data received")

	if ok, err := isDateCorrect(params.Newsletter.Payload.StartDatetime.String(), params.Newsletter.Payload.StopDatetime.String()); !ok {
		metrics.PostNewsletterHandlerErr.Inc()
		l.Errorf("error: %v", err)
		return operations.NewPutNewsletterIDBadRequest()
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	err := SHandler.DB.AddNewsletter(ctx, *params.Newsletter.Payload)
	if err != nil {
		metrics.PostNewsletterHandlerErr.Inc()
		l.Errorf("error to insert newsletter data, error:%v", err)
		return operations.NewPostNewsletterInternalServerError()
	}
	metrics.NewsletterRequestDurationSecond.With(prometheus.Labels{"method": "POST", "status": "201"}).Observe(time.Since(now).Seconds())
	l.Debug("Insert newsletter data success")
	return operations.NewPostNewsletterCreated()
}

func GetNewsletterHandler(params operations.GetNewsletterParams) middleware.Responder {
	metrics.GetNewsletterHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "GetNewsletterHandler",
		mymw.RequestContextID: reqID,
	})
	l.Info("request received")

	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	newsletters, err := SHandler.DB.GetNewsletter(ctx)
	if err != nil {
		metrics.GetNewsletterHandlerErr.Inc()
		l.Errorf("error to Select newsletter data, error:%v", err)
		return operations.NewGetNewsletterInternalServerError()
	}
	metrics.NewsletterRequestDurationSecond.With(prometheus.Labels{"method": "GET", "status": "200"}).Observe(time.Since(now).Seconds())

	l.Debug("Select newsletter data success")
	return operations.NewGetNewsletterOK().WithPayload(newsletters)
}

func GetNewsletterIDHandler(params operations.GetNewsletterIDParams) middleware.Responder {
	metrics.GetNewsletterIDHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "GetNewsletterIDHandler",
		mymw.RequestContextID: reqID,
	})
	l.Info("request received")

	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	newsletter, err := SHandler.DB.GetNewsletterByID(ctx, params.ID)
	if err != nil {
		metrics.GetNewsletterIDHandlerErr.Inc()
		l.Errorf("error to Select newsletter data by id %d, error:%v", params.ID, err)
		return operations.NewGetNewsletterInternalServerError()
	}
	if len(newsletter) == 0 {
		metrics.GetNewsletterIDHandlerErr.Inc()
		l.Infof("newletter with id %d has not been found", params.ID)
		return operations.NewGetNewsletterIDNotFound()
	}
	metrics.NewsletterRequestDurationSecond.With(prometheus.Labels{"method": "GET", "status": "200"}).Observe(time.Since(now).Seconds())
	l.Debugf("Select newsletter id %d success", params.ID)
	return operations.NewGetNewsletterIDOK().WithPayload(newsletter)
}

func DeleteNewsletterIDHandler(params operations.DeleteNewsletterIDParams, any interface{}) middleware.Responder {
	metrics.DeleteNewsletterIDHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "DeleteNewsletterIDHandler",
		mymw.RequestContextID: reqID,
	})
	l.Info("request received")
	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	err := SHandler.DB.DeleteNewsletter(ctx, params.ID)
	if err != nil {
		metrics.DeleteNewsletterIDHandlerErr.Inc()
		l.Errorf("error of deleting newsletter id %d from database, error:%v", params.ID, err)
		return operations.NewDeleteNewsletterIDInternalServerError()
	}
	metrics.NewsletterRequestDurationSecond.With(prometheus.Labels{"method": "DELETE", "status": "200"}).Observe(time.Since(now).Seconds())
	l.Infof("Newsletter id %d is deleted from the DB", params.ID)
	return operations.NewDeleteNewsletterIDOK()
}

func PutNewsletterIDHandler(params operations.PutNewsletterIDParams, any interface{}) middleware.Responder {
	metrics.PutNewsletterIDHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "PutNewsletterIDHandler",
		mymw.RequestContextID: reqID,
	})
	l.Debugf("input params %+v", params.Newsletter.Payload)
	l.Info("request received")

	if ok, err := isDateCorrect(params.Newsletter.Payload.StartDatetime.String(), params.Newsletter.Payload.StopDatetime.String()); !ok {
		metrics.PutNewsletterIDHandlerErr.Inc()
		l.Errorf("error: %v", err)
		return operations.NewPutNewsletterIDBadRequest()
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	if params.ID != params.Newsletter.Payload.ID {
		metrics.PutNewsletterIDHandlerErr.Inc()
		l.Errorf("Path id is not equal id in the payload %d!=%d", params.ID, params.Newsletter.Payload.ID)
		return operations.NewPutNewsletterIDBadRequest()
	}

	params.Newsletter.Payload.ID = params.ID
	err := SHandler.DB.UpdateNewsletter(ctx, params.Newsletter.Payload)
	if err != nil {
		metrics.PutNewsletterIDHandlerErr.Inc()
		l.Errorf("error of updating newsletter id %d from database, error:%v", params.ID, err)
		return operations.NewPutNewsletterIDInternalServerError()
	}
	metrics.NewsletterRequestDurationSecond.With(prometheus.Labels{"method": "PUT", "status": "201"}).Observe(time.Since(now).Seconds())
	l.Infof("Newsletter id %d is updated in the DB", params.ID)
	return operations.NewPutNewsletterIDCreated()
}

func isDateCorrect(startDateTime, stopDateTime string) (bool, error) {
	// layout := "2023-10-12T14:21:50.52+00:00"
	var err error
	var start, stop time.Time
	start, err = time.Parse(time.RFC3339, startDateTime)
	if err != nil {
		return false, errors.New("isDateCorrect startDateTime is not parseable")
	}
	stop, err = time.Parse(time.RFC3339, stopDateTime)
	if err != nil {
		return false, errors.New("isDateCorrect stopDateTime is not parseable")
	}
	if start.After(stop) {
		return false, errors.New("isDateCorrect stopDateTime early than startDateTime")
	}
	if start.Equal(stop) {
		return false, errors.New("isDateCorrect startDateTime is equal stopDateTime")
	}
	return true, nil

}
