package serviceHanlders

import (
	"backend/internal/metrics"
	"backend/models"

	"github.com/sirupsen/logrus"
)

func Authentication(user string, pass string) (interface{}, error) {
	metrics.AuthenticationCall.Inc()
	l := SHandler.Log.WithFields(logrus.Fields{
		"function": "Authentication",
	})
	l.Info("credentials received")

	if user == "demo" && pass == "p@55w0rd" {
		metrics.AuthenticationSuccess.Inc()
		l.Infof("User %s is authenticated successfully", user)
		return models.User{}, nil
	}
	metrics.AuthenticationFailed.Inc()
	l.Warnf("User is not authenticated %s", user)
	return nil, nil
}

func AlwaysAuthenticated(user string, pass string) (interface{}, error) {
	metrics.AuthenticationCall.Inc()
	l := SHandler.Log.WithFields(logrus.Fields{
		"function": "AlwaysAuthenticated",
	})
	l.Info("credentials received")

	metrics.AuthenticationSuccess.Inc()
	l.Infof("User %s is authenticated successfully", user)
	return models.User{}, nil
}
