package serviceHanlders

import (
	"context"
	"time"

	"backend/internal/handler/operations"
	mymw "backend/internal/middleware"

	"github.com/go-openapi/runtime/middleware"
	"github.com/sirupsen/logrus"
)

// Returns overall statistics
func GetStatisticsHandler(params operations.GetStatisticsParams) middleware.Responder {
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)
	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "GetStatisticsHandler",
		mymw.RequestContextID: reqID,
	})

	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	stat, err := SHandler.DB.GetStatistic(ctx)
	if err != nil {
		l.Errorf("error of selecting statistic from database, error:%v", err)
		return operations.NewGetStatisticsInternalServerError()
	}
	l.Info("Statistic is selected from the DB")
	return operations.NewGetStatisticsOK().WithPayload(stat)
}

// Returns statistics per ID
func GetStatisticsIDHandler(params operations.GetStatisticsIDParams) middleware.Responder {
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "GetStatisticsIDHandler",
		mymw.RequestContextID: reqID,
	})
	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	stat, err := SHandler.DB.GetStatisticID(ctx, params.ID)
	if err != nil {
		l.Errorf("error of selecting statistic by ID from database, error:%v", err)
		return operations.NewGetStatisticsIDInternalServerError()
	}
	l.Info("StatisticID is selected from the DB")
	return operations.NewGetStatisticsIDOK().WithPayload(stat)
}
