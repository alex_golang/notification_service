package serviceHanlders

import (
	"context"
	"time"

	"backend/internal/handler/operations"
	"backend/internal/metrics"
	mymw "backend/internal/middleware"

	"github.com/go-openapi/runtime/middleware"
	"github.com/sirupsen/logrus"
)

func PostTagsHandler(params operations.PostTagsParams, any interface{}) middleware.Responder {
	metrics.PostTagsHandlerCall.Inc()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)
	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "PostTagsHandler",
		mymw.RequestContextID: reqID,
	})
	l.Debugf("input params: %+v", params.TagName)

	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	err := SHandler.DB.AddTag(ctx, *params.TagName.TagName)
	if err != nil {
		metrics.PostTagsHandlerErr.Inc()
		l.Errorf("error to add new tag, error:%+v", err)
		return operations.NewPostTagsInternalServerError()
	}
	l.Info("new tag is created")
	return operations.NewPostTagsCreated()
}

func GetTagsHandler(params operations.GetTagsParams) middleware.Responder {
	metrics.GetTagsHandlerCall.Inc()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "GetTagsHandler",
		mymw.RequestContextID: reqID,
	})
	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	tags, err := SHandler.DB.GetTags(ctx)
	if err != nil {
		metrics.GetTagsHandlerErr.Inc()
		l.Errorf("error of selecting tags from database, error:%v", err)
		return operations.NewGetTagsInternalServerError()
	}
	l.Info("Tags is selected from the DB")
	return operations.NewGetTagsOK().WithPayload(tags)
}
