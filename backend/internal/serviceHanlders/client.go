package serviceHanlders

import (
	"context"
	"errors"
	"time"

	"backend/internal/handler/operations"
	"backend/internal/metrics"
	mymw "backend/internal/middleware"

	"github.com/go-openapi/runtime/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

func PostUserHandler(params operations.PostUserParams, any interface{}) middleware.Responder {
	metrics.PostUserHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "PostUserHandler",
		mymw.RequestContextID: reqID,
	})
	l.Debugf("input params: %+v", params)
	l.Info("data received")

	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	err := SHandler.DB.AddClient(ctx, *params.User.Payload)
	if err != nil {
		metrics.PostUserHandlerErr.Inc()
		l.Errorf("error to insert USER data, error:%+v", err)
		return operations.NewPostUserBadRequest()
	}
	metrics.UserRequestDurationSecond.With(prometheus.Labels{"method": "POST", "status": "201"}).Observe(time.Since(now).Seconds())
	l.Debug("Insert USER data success")
	return operations.NewPostUserCreated()
}

func GetUserHandler(params operations.GetUserParams) middleware.Responder {
	metrics.GetUserHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "GetUserHandler",
		mymw.RequestContextID: reqID,
	})
	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	clients, err := SHandler.DB.GetClients(ctx)
	if err != nil {
		metrics.GetUserHandlerErr.Inc()
		l.Errorf("error of selecting clients from database, error:%v", err)
		return operations.NewGetUserInternalServerError()
	}
	if errors.Is(err, errors.New("tag not exist")) {
		metrics.GetUserHandlerErr.Inc()
		return operations.NewGetNewsletterIDBadRequest()
	}
	metrics.UserRequestDurationSecond.With(prometheus.Labels{"method": "GET", "status": "200"}).Observe(time.Since(now).Seconds())
	l.Info("Data is selected from the DB")
	return operations.NewGetUserOK().WithPayload(clients)
}

func GetUserPhoneHandler(params operations.GetUserPhoneParams) middleware.Responder {
	metrics.GetUserPhoneHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "GetUserPhoneHandler",
		mymw.RequestContextID: reqID,
	})
	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	clients, err := SHandler.DB.GetClientByPhone(ctx, params.Phone)
	if err != nil {
		metrics.GetUserPhoneHandlerErr.Inc()
		l.Errorf("error of selecting client %s from database, error:%v", params.Phone, err)
		return operations.NewGetUserPhoneInternalServerError()
	}
	if len(clients) == 0 {
		metrics.GetUserPhoneHandlerErr.Inc()
		l.Infof("User %s has not been found", params.Phone)
		return operations.NewGetUserPhoneNotFound()
	}
	metrics.UserRequestDurationSecond.With(prometheus.Labels{"method": "GET", "status": "200"}).Observe(time.Since(now).Seconds())
	l.Infof("User %s is selected from the DB", params.Phone)
	return operations.NewGetUserPhoneOK().WithPayload(clients)
}

func DeleteUserPhoneHandler(params operations.DeleteUserPhoneParams, any interface{}) middleware.Responder {
	metrics.DeleteUserPhoneHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "DeleteUserPhoneHandler",
		mymw.RequestContextID: reqID,
	})
	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	err := SHandler.DB.DeleteClient(ctx, params.Phone)
	if err != nil {
		metrics.DeleteUserPhoneHandlerErr.Inc()
		l.Errorf("error of deleting client %s from database, error:%v", params.Phone, err)
		return operations.NewDeleteUserPhoneInternalServerError()
	}
	metrics.UserRequestDurationSecond.With(prometheus.Labels{"method": "DELETE", "status": "200"}).Observe(time.Since(now).Seconds())
	l.Infof("User %s deleted from the DB", params.Phone)
	return operations.NewDeleteUserPhoneOK()
}

func PutUserPhoneHandler(params operations.PutUserPhoneParams, any interface{}) middleware.Responder {
	metrics.PutUserPhoneHandlerCall.Inc()
	now := time.Now()
	reqID := params.HTTPRequest.Context().Value(mymw.RequestContextID)

	l := SHandler.Log.WithFields(logrus.Fields{
		"function":            "PutUserPhoneHandler",
		mymw.RequestContextID: reqID,
	})
	l.Debugf("input params %+v", params.User.Payload)
	if params.Phone != *params.User.Payload.Phone {
		metrics.PutUserPhoneHandlerErr.Inc()
		l.Errorf("phone in path (%s) is not equal phone (%s) in body", *params.User.Payload.Phone, params.Phone)
		return operations.NewPutUserPhoneBadRequest()
	}
	ctx, _ := context.WithTimeout(context.Background(), time.Second*SHandler.Timeout)
	err := SHandler.DB.UpdateClient(ctx, params.User.Payload)
	if err != nil {
		metrics.PutUserPhoneHandlerErr.Inc()
		l.Errorf("error of updating the client %s from database, error:%v", *params.User.Payload.Phone, err)
		return operations.NewPutUserPhoneInternalServerError()
	}
	metrics.UserRequestDurationSecond.With(prometheus.Labels{"method": "PUT", "status": "201"}).Observe(time.Since(now).Seconds())
	l.Infof("Update the client %s is selected from the DB", *params.User.Payload.Phone)
	return operations.NewPutUserPhoneCreated()
}
