package middleware

import (
	"backend/internal/metrics"
	"context"
	"net/http"

	guid "github.com/satori/go.uuid"
)

const RequestContextID = "requestID"

func ContextRequestMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ctx context.Context

		reqID := r.Header.Get("X-Request-ID")
		if reqID == "" {
			uuid := guid.Must(guid.NewV4(), nil)
			reqID = uuid.String()
		}

		ctx = context.WithValue(context.TODO(), RequestContextID, reqID)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func MetricTotalRequestsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		metrics.TotalRequestsCall.Inc()
		next.ServeHTTP(w, r)
	})
}
