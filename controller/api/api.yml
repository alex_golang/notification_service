swagger: "2.0"
info:
  version: "1.0.0"
  title: "Notification Service"
  description: "Service of newsletter management, admin and statistic API"

produces:
  - application/json
        
securityDefinitions:
  basicAuth:
    type: basic

paths:
  /user:
    post:
      summary: "Create user"
      security:
        - basicAuth: []        
      parameters:
        - in: body
          name: user
          description: "User data"
          required: true
          schema:
           $ref: "#/definitions/CreateUserRequest"
      responses:
        "201":
          description: "User sucessfully created"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"
        "500":
          description: "Internal Server Error"
    get:
      summary: Get user list
      responses:
        "200":
          description: Ok
          schema:
            type: array
            items:
              $ref: "#/definitions/User"
        "500":
          description: "Internal Server Error"
  /user/{phone}:
    put:
      summary: "Change user"
      security:
        - basicAuth: []        
      parameters:
        - in: path
          name: phone
          type: string
          description: "user mobile phone that will be updated"
          required: true
        - in: body
          name: user
          description: "user data"
          required: true
          schema:
           $ref: "#/definitions/CreateUserRequest"
      responses:
        "201":
          description: "User sucessfully modified"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"
        "500":
          description: "Internal Server Error"
    delete:
      summary: "Delete user"
      security:
        - basicAuth: []        
      parameters:
        - in: path
          name: phone
          type: string
          description: "user mobile phone"
          required: true
      responses:
        "200":
          description: "OK"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"
        "500":
          description: "Internal Server Error"
    get:
      summary: "Get user by mobile phone"
      parameters:
        - in: path
          name: phone
          type: string
          description: "user mobile phone"
          required: true
      responses:
        "200":
          description: Ok
          schema:
            type: array 
            items:
              $ref: "#/definitions/User"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"
        "404":
          description: "Not found"
        "500":
          description: "Internal Server Error"
  /newsletter:
    post:
      summary: "Create newsletter"
      security:
        - basicAuth: []        
      parameters:
        - in: body
          name: newsletter
          description: "newsletter data"
          required: true
          schema:
           $ref: "#/definitions/CreateNewsletter"
      responses:
        "201":
          description: "Newsletter sucessfully created"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"
        "500":
          description: "Internal Server Error"
    get:
      summary: Get newsletter list
      responses:
        "200":
          description: Ok
          schema:
            type: array
            items:
              $ref: "#/definitions/Newsletter"
        "401":
          description: "Not authorized"
        "500":
          description: "Internal Server Error"
  /newsletter/{id}:
    put:
      summary: "Change/update newsletter"
      security:
        - basicAuth: []
      parameters:
        - in: path
          name: id
          type: integer
          description: "Newsletter id that will be updated"
          required: true
        - in: body
          name: newsletter
          description: "newsletter data"
          required: true
          schema:
           $ref: "#/definitions/CreateNewsletter"
      responses:
        "201":
          description: "Newsletter successfully modified"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"        
        "500":
          description: "Internal Server Error"
    delete:
      summary: "Delete newsletter by ID"
      security:
        - basicAuth: []        
      parameters:
        - in: path
          name: id
          type: integer
          description: "Newsletter id"
          required: true
      responses:
        "200":
          description: "OK"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"
        "500":
          description: "Internal Server Error"
    get:
      summary: "Get newsletter by ID"
      parameters:
        - in: path
          name: id
          type: integer
          description: "Get newsletter by id"
          required: true
      responses:
        "200":
          description: Ok
          schema:
            type: array 
            items:
              $ref: "#/definitions/Newsletter"
        "400":
          description: "Bad request"
        "401":
          description: "Not authorized"
        "404":
          description: "Not found"  
        "500":
          description: "Internal Server Error"
  /statistics:
    get:
      summary: "Get common statistics"
      responses:
        "200":
          description: Ok
          schema:
            $ref: "#/definitions/Statistics"
        "500":
          description: "Internal Server Error"
  /statistics/{id}:
    get:
      summary: "Get statistics by newsletter ID"
      parameters:
        - in: path
          name: id
          type: integer
          description: "Get statistics by id"
          required: true
      responses:
        "200":
          description: Ok
          schema:
            type: array 
            items:
              $ref: "#/definitions/StatisticsID"
        "400":
          description: "Bad request"
        "500":
          description: "Internal Server Error"
  /tags:
    get:
      summary: "Get list of tags"
      responses:
        "200":
          description: OK
          schema:
            type: array
            items:
              $ref: "#/definitions/Tags"
        "500":
          description: "Internal Server Error"
    post:
      summary: "Create new tag"
      security:
        - basicAuth: []
      parameters:
        - in: body
          name: tag_name
          description: "tag name"
          required: true
          schema:
            $ref: "#/definitions/Tags"
      responses:
        "201":
          description: "Created new tag"
        "401":
          description: "Not authorized"   
        "500":
          description: "Internal Server Error"            
definitions:
  Tags:
    description: "List of available tags"
    type: object
    required:
      - tag_name
    properties:
      tag_id:
        description: "ID of tag, range [1-899]"
        type: integer
        minimum: 1
        maximum: 899
      tag_name:
        description: "Name of tag"
        type: string
        minLength: 3
        maxLength: 100
        example: "воздух"

  CreateUserRequest:
    description: "Create user request params"
    type: object
    required:
      - payload
    properties:
      payload:
        $ref: "#/definitions/User"
  User:
    description: User model
    type: object
    required:
      - phone
      - tags
      - timezone
    properties:
      phone:
        description: "Mobile phone"
        type: string
        minLength: 11
        maxLength: 11
        pattern: ^7\d{10}$
        example: "7XXXXXXXXXX (X - цифра от 0 до 9)"
      tags:
        description: "List of tags_IDs"
        type: array
        items:
          type: integer
        example: 1-дрова,2-опилки, 3-бревна '[1,2,3]'
      timezone:
        description: "Time zone of the user"
        type: string
        example: "UTC+3"
  

  CreateNewsletter:
    description: "Create new newsletter"
    type: object
    required:
      - payload
    properties:
      payload:
        $ref: "#/definitions/Newsletter"

  Newsletter:
    description: Newsletter model
    type: object
    required:
      - start_datetime
      - stop_datetime
      - text
      - filter_type
      - filter
    properties:
      id:
        description: "ID of the newsletter"
        type: integer
        example: "123"
      start_datetime:
        description: "Mailing start time"
        type: string
        format: date-time
        example: "2019-10-12T14:20:50.52+03:00 (UTC+3)"
      stop_datetime:
        description: "Mailing stop time"
        type: string
        format: date-time
        example: "2019-10-12T14:21:50.52+03:00 (UTC+3)"
      filter_type:
        description: "Filter properties of clients to whom the mailing should be carried out (mobile operator code - moc, tag, both)"
        type: string
        pattern: '^(moc)$|(tag)$|(both)$'
      filter:
        description: "The filter itself"
        type: array
        items:
          type: integer
      text:
        description: "Text of the newsletter"
        type: string
        minLength: 10
        example: "Text of the message"
  

  Statistics:
    description: "Shows overall statistics"
    type: object
    required:
      - payload
    properties:
      payload:
        $ref: "#/definitions/OverallStatistics"
  
  OverallStatistics:
    description: "Over all statistics, just show count of newsletters and total message count"
    type: object
    required:
      - newsletter_count
      - ok_message_count
      - nok_message_count
    properties:
      newsletter_count:
        description: "Total number of newsletter in the system"
        type: integer
        example: "100"
      ok_message_count:
        description: "Total number of successfully sent messages"
        type: integer
        example: "1012"
      nok_message_count:
        description: "Total number of messages sent unsuccessfully"
        type: integer
        example: "13"

  StatisticsID:
    description: "Shows statistics per newsletter id"
    type: object
    required:
      - payload
    properties:
      payload:
        $ref: "#/definitions/StatisticsPerID"
  
  StatisticsPerID:
    description: Statistics per newsletter id
    type: object
    required:
      - newsletter_id
      - ok_message_count
      - nok_message_count
    properties:
      newsletter_id:
        description: "Newsletter ID"
        type: integer
        example: "10"
      ok_message_count:
        description: "Number of successfully sent messages for the newsletter"
        type: integer
        example: "503"
      nok_message_count:
        description: "Number of successfully sent messages for the newsletter"
        type: integer
        example: "12"