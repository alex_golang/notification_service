// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"controller/models"
)

// GetUserPhoneReader is a Reader for the GetUserPhone structure.
type GetUserPhoneReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *GetUserPhoneReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewGetUserPhoneOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewGetUserPhoneBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 401:
		result := NewGetUserPhoneUnauthorized()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 404:
		result := NewGetUserPhoneNotFound()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	case 500:
		result := NewGetUserPhoneInternalServerError()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewGetUserPhoneOK creates a GetUserPhoneOK with default headers values
func NewGetUserPhoneOK() *GetUserPhoneOK {
	return &GetUserPhoneOK{}
}

/*
GetUserPhoneOK describes a response with status code 200, with default header values.

Ok
*/
type GetUserPhoneOK struct {
	Payload []*models.User
}

// IsSuccess returns true when this get user phone o k response has a 2xx status code
func (o *GetUserPhoneOK) IsSuccess() bool {
	return true
}

// IsRedirect returns true when this get user phone o k response has a 3xx status code
func (o *GetUserPhoneOK) IsRedirect() bool {
	return false
}

// IsClientError returns true when this get user phone o k response has a 4xx status code
func (o *GetUserPhoneOK) IsClientError() bool {
	return false
}

// IsServerError returns true when this get user phone o k response has a 5xx status code
func (o *GetUserPhoneOK) IsServerError() bool {
	return false
}

// IsCode returns true when this get user phone o k response a status code equal to that given
func (o *GetUserPhoneOK) IsCode(code int) bool {
	return code == 200
}

// Code gets the status code for the get user phone o k response
func (o *GetUserPhoneOK) Code() int {
	return 200
}

func (o *GetUserPhoneOK) Error() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneOK  %+v", 200, o.Payload)
}

func (o *GetUserPhoneOK) String() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneOK  %+v", 200, o.Payload)
}

func (o *GetUserPhoneOK) GetPayload() []*models.User {
	return o.Payload
}

func (o *GetUserPhoneOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	// response payload
	if err := consumer.Consume(response.Body(), &o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewGetUserPhoneBadRequest creates a GetUserPhoneBadRequest with default headers values
func NewGetUserPhoneBadRequest() *GetUserPhoneBadRequest {
	return &GetUserPhoneBadRequest{}
}

/*
GetUserPhoneBadRequest describes a response with status code 400, with default header values.

Bad request
*/
type GetUserPhoneBadRequest struct {
}

// IsSuccess returns true when this get user phone bad request response has a 2xx status code
func (o *GetUserPhoneBadRequest) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this get user phone bad request response has a 3xx status code
func (o *GetUserPhoneBadRequest) IsRedirect() bool {
	return false
}

// IsClientError returns true when this get user phone bad request response has a 4xx status code
func (o *GetUserPhoneBadRequest) IsClientError() bool {
	return true
}

// IsServerError returns true when this get user phone bad request response has a 5xx status code
func (o *GetUserPhoneBadRequest) IsServerError() bool {
	return false
}

// IsCode returns true when this get user phone bad request response a status code equal to that given
func (o *GetUserPhoneBadRequest) IsCode(code int) bool {
	return code == 400
}

// Code gets the status code for the get user phone bad request response
func (o *GetUserPhoneBadRequest) Code() int {
	return 400
}

func (o *GetUserPhoneBadRequest) Error() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneBadRequest ", 400)
}

func (o *GetUserPhoneBadRequest) String() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneBadRequest ", 400)
}

func (o *GetUserPhoneBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetUserPhoneUnauthorized creates a GetUserPhoneUnauthorized with default headers values
func NewGetUserPhoneUnauthorized() *GetUserPhoneUnauthorized {
	return &GetUserPhoneUnauthorized{}
}

/*
GetUserPhoneUnauthorized describes a response with status code 401, with default header values.

Not authorized
*/
type GetUserPhoneUnauthorized struct {
}

// IsSuccess returns true when this get user phone unauthorized response has a 2xx status code
func (o *GetUserPhoneUnauthorized) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this get user phone unauthorized response has a 3xx status code
func (o *GetUserPhoneUnauthorized) IsRedirect() bool {
	return false
}

// IsClientError returns true when this get user phone unauthorized response has a 4xx status code
func (o *GetUserPhoneUnauthorized) IsClientError() bool {
	return true
}

// IsServerError returns true when this get user phone unauthorized response has a 5xx status code
func (o *GetUserPhoneUnauthorized) IsServerError() bool {
	return false
}

// IsCode returns true when this get user phone unauthorized response a status code equal to that given
func (o *GetUserPhoneUnauthorized) IsCode(code int) bool {
	return code == 401
}

// Code gets the status code for the get user phone unauthorized response
func (o *GetUserPhoneUnauthorized) Code() int {
	return 401
}

func (o *GetUserPhoneUnauthorized) Error() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneUnauthorized ", 401)
}

func (o *GetUserPhoneUnauthorized) String() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneUnauthorized ", 401)
}

func (o *GetUserPhoneUnauthorized) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetUserPhoneNotFound creates a GetUserPhoneNotFound with default headers values
func NewGetUserPhoneNotFound() *GetUserPhoneNotFound {
	return &GetUserPhoneNotFound{}
}

/*
GetUserPhoneNotFound describes a response with status code 404, with default header values.

Not found
*/
type GetUserPhoneNotFound struct {
}

// IsSuccess returns true when this get user phone not found response has a 2xx status code
func (o *GetUserPhoneNotFound) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this get user phone not found response has a 3xx status code
func (o *GetUserPhoneNotFound) IsRedirect() bool {
	return false
}

// IsClientError returns true when this get user phone not found response has a 4xx status code
func (o *GetUserPhoneNotFound) IsClientError() bool {
	return true
}

// IsServerError returns true when this get user phone not found response has a 5xx status code
func (o *GetUserPhoneNotFound) IsServerError() bool {
	return false
}

// IsCode returns true when this get user phone not found response a status code equal to that given
func (o *GetUserPhoneNotFound) IsCode(code int) bool {
	return code == 404
}

// Code gets the status code for the get user phone not found response
func (o *GetUserPhoneNotFound) Code() int {
	return 404
}

func (o *GetUserPhoneNotFound) Error() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneNotFound ", 404)
}

func (o *GetUserPhoneNotFound) String() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneNotFound ", 404)
}

func (o *GetUserPhoneNotFound) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}

// NewGetUserPhoneInternalServerError creates a GetUserPhoneInternalServerError with default headers values
func NewGetUserPhoneInternalServerError() *GetUserPhoneInternalServerError {
	return &GetUserPhoneInternalServerError{}
}

/*
GetUserPhoneInternalServerError describes a response with status code 500, with default header values.

Internal Server Error
*/
type GetUserPhoneInternalServerError struct {
}

// IsSuccess returns true when this get user phone internal server error response has a 2xx status code
func (o *GetUserPhoneInternalServerError) IsSuccess() bool {
	return false
}

// IsRedirect returns true when this get user phone internal server error response has a 3xx status code
func (o *GetUserPhoneInternalServerError) IsRedirect() bool {
	return false
}

// IsClientError returns true when this get user phone internal server error response has a 4xx status code
func (o *GetUserPhoneInternalServerError) IsClientError() bool {
	return false
}

// IsServerError returns true when this get user phone internal server error response has a 5xx status code
func (o *GetUserPhoneInternalServerError) IsServerError() bool {
	return true
}

// IsCode returns true when this get user phone internal server error response a status code equal to that given
func (o *GetUserPhoneInternalServerError) IsCode(code int) bool {
	return code == 500
}

// Code gets the status code for the get user phone internal server error response
func (o *GetUserPhoneInternalServerError) Code() int {
	return 500
}

func (o *GetUserPhoneInternalServerError) Error() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneInternalServerError ", 500)
}

func (o *GetUserPhoneInternalServerError) String() string {
	return fmt.Sprintf("[GET /user/{phone}][%d] getUserPhoneInternalServerError ", 500)
}

func (o *GetUserPhoneInternalServerError) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
