// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// NewDeleteNewsletterIDParams creates a new DeleteNewsletterIDParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewDeleteNewsletterIDParams() *DeleteNewsletterIDParams {
	return &DeleteNewsletterIDParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteNewsletterIDParamsWithTimeout creates a new DeleteNewsletterIDParams object
// with the ability to set a timeout on a request.
func NewDeleteNewsletterIDParamsWithTimeout(timeout time.Duration) *DeleteNewsletterIDParams {
	return &DeleteNewsletterIDParams{
		timeout: timeout,
	}
}

// NewDeleteNewsletterIDParamsWithContext creates a new DeleteNewsletterIDParams object
// with the ability to set a context for a request.
func NewDeleteNewsletterIDParamsWithContext(ctx context.Context) *DeleteNewsletterIDParams {
	return &DeleteNewsletterIDParams{
		Context: ctx,
	}
}

// NewDeleteNewsletterIDParamsWithHTTPClient creates a new DeleteNewsletterIDParams object
// with the ability to set a custom HTTPClient for a request.
func NewDeleteNewsletterIDParamsWithHTTPClient(client *http.Client) *DeleteNewsletterIDParams {
	return &DeleteNewsletterIDParams{
		HTTPClient: client,
	}
}

/*
DeleteNewsletterIDParams contains all the parameters to send to the API endpoint

	for the delete newsletter ID operation.

	Typically these are written to a http.Request.
*/
type DeleteNewsletterIDParams struct {

	/* ID.

	   Newsletter id
	*/
	ID int64

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the delete newsletter ID params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *DeleteNewsletterIDParams) WithDefaults() *DeleteNewsletterIDParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the delete newsletter ID params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *DeleteNewsletterIDParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) WithTimeout(timeout time.Duration) *DeleteNewsletterIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) WithContext(ctx context.Context) *DeleteNewsletterIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) WithHTTPClient(client *http.Client) *DeleteNewsletterIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) WithID(id int64) *DeleteNewsletterIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the delete newsletter ID params
func (o *DeleteNewsletterIDParams) SetID(id int64) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteNewsletterIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt64(o.ID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
