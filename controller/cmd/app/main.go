package main

import (
	"controller/internal/authenticator"
	"controller/internal/routes"
	"log"
	"net/http"
)

func main() {

	auth, err := authenticator.New()
	if err != nil {
		log.Fatalf("Failed to initialize the authenticator: %v", err)
	}

	rtr := routes.New(auth)

	log.Print("Server listening on http://localhost:3030/")
	if err := http.ListenAndServe("0.0.0.0:3030", rtr); err != nil {
		log.Fatalf("There was an error with the http server: %v", err)
	}
}
