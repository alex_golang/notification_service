// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// User User model
//
// swagger:model User
type User struct {

	// Mobile phone
	// Example: 7XXXXXXXXXX (X - цифра от 0 до 9)
	// Required: true
	// Max Length: 11
	// Min Length: 11
	// Pattern: ^7\d{10}$
	Phone *string `json:"phone"`

	// List of tags_IDs
	// Example: 1-дрова,2-опилки, 3-бревна '[1,2,3]'
	// Required: true
	Tags []int64 `json:"tags"`

	// Time zone of the user
	// Example: UTC+3
	// Required: true
	Timezone *string `json:"timezone"`
}

// Validate validates this user
func (m *User) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validatePhone(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateTags(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateTimezone(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *User) validatePhone(formats strfmt.Registry) error {

	if err := validate.Required("phone", "body", m.Phone); err != nil {
		return err
	}

	if err := validate.MinLength("phone", "body", *m.Phone, 11); err != nil {
		return err
	}

	if err := validate.MaxLength("phone", "body", *m.Phone, 11); err != nil {
		return err
	}

	if err := validate.Pattern("phone", "body", *m.Phone, `^7\d{10}$`); err != nil {
		return err
	}

	return nil
}

func (m *User) validateTags(formats strfmt.Registry) error {

	if err := validate.Required("tags", "body", m.Tags); err != nil {
		return err
	}

	return nil
}

func (m *User) validateTimezone(formats strfmt.Registry) error {

	if err := validate.Required("timezone", "body", m.Timezone); err != nil {
		return err
	}

	return nil
}

// ContextValidate validates this user based on context it is used
func (m *User) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *User) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *User) UnmarshalBinary(b []byte) error {
	var res User
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
