package routes

//	httptransport "github.com/go-openapi/runtime/client"

import (
	"controller/client"
	"controller/internal/authenticator"
	"controller/internal/callback"
	"controller/internal/handlers"
	"controller/internal/login"
	"controller/internal/logout"
	"controller/internal/middleware"
	"controller/internal/user"
	"encoding/gob"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
	"github.com/sirupsen/logrus"
)

// New registers the routes and returns the router.
func New(auth *authenticator.Authenticator) *gin.Engine {
	httpClient := &http.Client{Timeout: time.Second * 5}
	h := &handlers.Handler{HttpClient: httpClient}
	logger := logrus.New()
	h.Log = logger

	backendAddr := fmt.Sprintf("%s:%s", os.Getenv("BACKEND_NAME"), os.Getenv("API_PORT"))

	transport := httptransport.New(backendAddr, client.DefaultBasePath, []string{"http"})
	apiclient := client.New(transport, strfmt.Default)
	apiclient.SetTransport(transport)
	h.ApiClient = apiclient

	router := gin.Default()

	gob.Register(map[string]interface{}{})

	store := cookie.NewStore([]byte("secret"))
	router.Use(sessions.Sessions("auth-session", store))

	// router.Static("/public", "web/static")
	router.LoadHTMLGlob("web/templates/*")

	router.GET("/", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "main.html", nil)
	})
	statRoutes := router.Group("/stat", middleware.IsAuthenticated)
	statRoutes.GET("/", h.GetStat)
	statRoutes.GET("/:id", h.GetStatID)

	userRoutes := router.Group("/user")
	userRoutes.GET("/", h.GetUser)
	userRoutes.GET("/:phone", h.GetUserPhone)
	userRoutes.POST("/", middleware.IsAuthenticated, h.CreateUser)
	userRoutes.PUT(`/:phone`, middleware.IsAuthenticated, h.ChangeUser)
	userRoutes.DELETE(`/:phone`, middleware.IsAuthenticated, h.DeleteUser)

	newsletterRoutes := router.Group("/newsletter")
	newsletterRoutes.GET("/", h.GetNewsletter)
	newsletterRoutes.GET(`/:id`, h.GetNewsletterID)
	newsletterRoutes.POST("/", middleware.IsAuthenticated, h.CreateNewsletter)
	newsletterRoutes.PUT(`/:id`, middleware.IsAuthenticated, h.ChangeNewsletter)
	newsletterRoutes.DELETE(`/:id`, middleware.IsAuthenticated, h.DeleteNewsletter)

	tagsRoutes := router.Group("/tags")
	tagsRoutes.GET("/", h.GetTag)
	tagsRoutes.POST("/", middleware.IsAuthenticated, h.CreateTag)

	router.GET("/login", login.Handler(auth))
	router.GET("/callback", callback.Handler(auth))
	router.GET("/account", middleware.IsAuthenticated, user.Handler)
	router.GET("/logout", logout.Handler)

	return router
}
