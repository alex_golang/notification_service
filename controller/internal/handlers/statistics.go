package handlers

import (
	"context"
	"controller/client/operations"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// type Stat struct {
// 	Id   int64
// 	Name string
// }

func (h *Handler) GetStat(ctx *gin.Context) {

	l := h.Log.WithField("function", "GetStat")
	stats, err := h.ApiClient.Operations.GetStatistics(nil)
	if err != nil {
		l.Errorf("Error getting statistics from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Error getting statistics from backend",
		})
		return
	}
	ctx.HTML(http.StatusOK, "statistic.html", gin.H{
		"Title":  "Общая статистика",
		"Ncount": stats.Payload.Payload.NewsletterCount,
		"Ok":     stats.Payload.Payload.OkMessageCount,
		"Nok":    stats.Payload.Payload.NokMessageCount,
	})
}

func (h *Handler) GetStatID(ctx *gin.Context) {

	l := h.Log.WithField("function", "GetStatID")
	ctx1, _ := context.WithTimeout(context.Background(), time.Duration(3*time.Second))
	Id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		l.Errorf("Error getting statistics by id due to wrong id number %v", err)
		ctx.HTML(http.StatusBadRequest, "error.html", gin.H{
			"Title": "Error 400",
			"Error": "wrong id number",
		})
		return
	}
	params := operations.GetStatisticsIDParams{
		ID:         int64(Id),
		Context:    ctx1,
		HTTPClient: h.HttpClient,
	}

	stats, err := h.ApiClient.Operations.GetStatisticsID(&params)
	if err != nil {
		l.Errorf("Error getting statistics from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Error getting statistics from backend",
		})
		return
	}
	if len(stats.Payload) != 1 {
		l.Errorf("Error getting statistics from backend %v", err)
		ctx.HTML(http.StatusBadRequest, "error.html", gin.H{
			"Title": "Error 400",
			"Error": fmt.Sprintf("The newsletter with ID %d not found", Id),
		})
		return
	}
	ctx.HTML(http.StatusOK, "statistic-id.html", gin.H{
		"Title": "Статистика по рассылке",
		"NID":   Id,
		"Ok":    stats.Payload[0].Payload.OkMessageCount,
		"Nok":   stats.Payload[0].Payload.NokMessageCount,
	})
}
