package handlers

import (
	"controller/client"
	"encoding/base64"
	"net/http"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"github.com/sirupsen/logrus"
)

type Handler struct {
	ApiClient  *client.NotificationService
	HttpClient *http.Client
	Log        *logrus.Logger
}

// BasicAuth provides a basic auth info writer
func BasicAuth(username, password string) runtime.ClientAuthInfoWriter {
	return runtime.ClientAuthInfoWriterFunc(func(r runtime.ClientRequest, _ strfmt.Registry) error {
		encoded := base64.StdEncoding.EncodeToString([]byte(username + ":" + password))
		return r.SetHeaderParam(runtime.HeaderAuthorization, "Basic "+encoded)
	})
}
