package handlers

import (
	"context"
	"controller/client/operations"
	"net/http"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

type User struct {
	Phone      string
	MobileCode string
	Tags       []Tags
	Timezone   string
}

func (h *Handler) GetUser(ctx *gin.Context) {
	l := h.Log.WithField("function", "GetUser")

	users, err := h.ApiClient.Operations.GetUser(nil)
	if err != nil {
		l.Errorf("Error getting users from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Communication server error",
		})
	}
	l.Debugf("Selected %d users", len(users.Payload))

	tags, err := h.ApiClient.Operations.GetTags(nil)
	if err != nil {
		l.Errorf("Error getting tag from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Communication server error",
		})
	}
	l.Debugf("Selected %d tags", len(tags.Payload))

	TagMap := make(map[int64]string)
	for _, val := range tags.Payload {
		if _, exist := TagMap[val.TagID]; !exist {
			TagMap[val.TagID] = *val.TagName
		}
	}
	resultUsers := make([]User, 0, len(users.Payload))
	for _, val := range users.Payload {
		phone := *val.Phone
		tags := make([]Tags, 0, len(val.Tags))
		for _, tag := range val.Tags {
			tags = append(tags, Tags{Id: tag, Name: TagMap[tag]})
		}
		user := User{
			Phone:      phone,
			MobileCode: phone[1:4],
			Tags:       tags,
			Timezone:   *val.Timezone,
		}
		resultUsers = append(resultUsers, user)
	}

	auth := false
	if sessions.Default(ctx).Get("profile") != nil {
		auth = true
	}

	ctx.HTML(http.StatusOK, "client.html", gin.H{
		"Title":         "User list",
		"Users":         resultUsers,
		"CreatedTag":    false,
		"Authenticated": auth,
	})
}

func (h *Handler) GetUserPhone(ctx *gin.Context) {
	l := h.Log.WithField("function", "GetUserPhone")
	ctx1, _ := context.WithTimeout(context.Background(), time.Duration(3*time.Second))

	phoneParams := operations.GetUserPhoneParams{
		Phone:      ctx.Param("phone"),
		Context:    ctx1,
		HTTPClient: h.HttpClient,
	}
	user, err := h.ApiClient.Operations.GetUserPhone(&phoneParams)
	if err != nil {
		l.Errorf("Error getting users from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Communication server error",
		})
		return
	}

	tags, err := h.ApiClient.Operations.GetTags(nil)
	if err != nil {
		l.Errorf("Error getting tag from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Communication server error",
		})
		return
	}
	l.Debugf("Selected %d tags", len(tags.Payload))

	TagMap := make(map[int64]string)
	for _, val := range tags.Payload {
		if _, exist := TagMap[val.TagID]; !exist {
			TagMap[val.TagID] = *val.TagName
		}
	}
	resultUsers := make([]User, 0, len(user.Payload))
	for _, val := range user.Payload {
		phone := *val.Phone
		tags := make([]Tags, 0, len(val.Tags))
		for _, tag := range val.Tags {
			tags = append(tags, Tags{Id: tag, Name: TagMap[tag]})
		}
		user := User{
			Phone:      phone,
			MobileCode: phone[1:4],
			Tags:       tags,
			Timezone:   *val.Timezone,
		}
		resultUsers = append(resultUsers, user)
	}

	auth := false
	if sessions.Default(ctx).Get("profile") != nil {
		auth = true
	}

	ctx.HTML(http.StatusOK, "client.html", gin.H{
		"Title":         "User list",
		"Users":         resultUsers,
		"CreatedTag":    false,
		"Authenticated": auth,
	})
}

func (h *Handler) CreateUser(ctx *gin.Context) {
	ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
		"Title": "Error 501",
		"Error": "Not implemented",
	})
}

func (h *Handler) ChangeUser(ctx *gin.Context) {
	ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
		"Title": "Error 501",
		"Error": "Not implemented",
	})
}

func (h *Handler) DeleteUser(ctx *gin.Context) {
	ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
		"Title": "Error 501",
		"Error": "Not implemented",
	})
}
