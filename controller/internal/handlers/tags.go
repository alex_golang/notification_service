package handlers

import (
	"context"
	"controller/client/operations"
	"controller/models"
	"net/http"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

type Tags struct {
	Id   int64
	Name string
}

func (h *Handler) GetTag(ctx *gin.Context) {
	l := h.Log.WithField("function", "GetTag")
	tags, err := h.ApiClient.Operations.GetTags(nil)
	if err != nil {
		l.Errorf("Error getting tag from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "tags.html", nil)
	}
	l.Debugf("Selected %d tags", len(tags.Payload))
	resultTag := make([]Tags, 0, len(tags.Payload))
	for _, val := range tags.Payload {
		resultTag = append(resultTag, Tags{Id: val.TagID, Name: *val.TagName})
	}

	auth := false
	if sessions.Default(ctx).Get("profile") != nil {
		auth = true
	}

	ctx.HTML(http.StatusOK, "tags.html", gin.H{
		"Title":         "Tag list",
		"Tags":          resultTag,
		"CreatedTag":    false,
		"Authenticated": auth,
	})
}

func (h *Handler) CreateTag(ctx *gin.Context) {
	l := h.Log.WithField("function", "CreateTag")

	var result bool = false
	tagName := ctx.Request.FormValue("tagname")
	// user := ctx.Request.FormValue("login")
	// pass := ctx.Request.FormValue("password")
	user := "login"
	pass := "password"

	authBasic := BasicAuth(user, pass)
	ctx1, _ := context.WithTimeout(context.Background(), time.Duration(3*time.Second))
	params := &operations.PostTagsParams{
		TagName:    &models.Tags{TagName: &tagName},
		Context:    ctx1,
		HTTPClient: h.HttpClient,
	}
	res, err := h.ApiClient.Operations.PostTags(params, authBasic)
	if res.Code() == 401 {
		l.Errorf("authentication error %v", err) //TODO: change the API template
		ctx.HTML(http.StatusUnauthorized, "tags.html", nil)
	}
	if err != nil {
		l.Errorf("Error creating new tag  %v", err)
		ctx.HTML(http.StatusInternalServerError, "tags.html", nil)
	}
	if res.Code() == 201 {
		result = true
	}

	tags, err := h.ApiClient.Operations.GetTags(nil)
	if err != nil {
		l.Errorf("Error receiving tag from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "tags.html", nil)
	}
	l.Debugf("Selected %d tags", len(tags.Payload))

	resultTag := make([]Tags, 0, len(tags.Payload))
	for _, val := range tags.Payload {
		resultTag = append(resultTag, Tags{Id: val.TagID, Name: *val.TagName})
	}

	auth := false
	if sessions.Default(ctx).Get("profile") != nil {
		auth = true
	}
	ctx.HTML(http.StatusOK, "tags.html", gin.H{
		"Title":         "Tag list",
		"Tags":          resultTag,
		"CreatedTag":    result,
		"Authenticated": auth,
	})
}
