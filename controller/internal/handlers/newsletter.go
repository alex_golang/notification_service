package handlers

import (
	"context"
	"controller/client/operations"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type NewsLetter struct {
	ID         int64
	Start      *time.Time
	Stop       *time.Time
	FilterType string
	Filter     []int64
}

func (h *Handler) GetNewsletter(ctx *gin.Context) {
	l := h.Log.WithField("function", "GetNewsletter")

	newsletters, err := h.ApiClient.Operations.GetNewsletter(nil)
	if err != nil {
		l.Errorf("Error getting newsletters from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Communication server error",
		})
	}
	l.Debugf("Selected %d newsletters", len(newsletters.Payload))

	resultNewsletter := make([]NewsLetter, 0, len(newsletters.Payload))
	for _, val := range newsletters.Payload {
		news := NewsLetter{
			ID:         val.ID,
			Start:      (*time.Time)(val.StartDatetime),
			Stop:       (*time.Time)(val.StopDatetime),
			FilterType: *val.FilterType,
			Filter:     val.Filter,
		}
		resultNewsletter = append(resultNewsletter, news)
	}

	ctx.HTML(http.StatusOK, "newsletter.html", gin.H{
		"Title":                "Newsletter list",
		"Newsletters":          resultNewsletter,
		"NewsletterCreatedTag": false,
	})
}

func (h *Handler) GetNewsletterID(ctx *gin.Context) {
	l := h.Log.WithField("function", "GetNewsletter")

	ctx1, _ := context.WithTimeout(context.Background(), time.Duration(3*time.Second))
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.HTML(http.StatusBadRequest, "error.html", gin.H{
			"Title": "Error 400",
			"Error": "wrong id number",
		})
		return
	}

	newsParams := operations.GetNewsletterIDParams{
		ID:         int64(id),
		Context:    ctx1,
		HTTPClient: h.HttpClient,
	}
	newsletters, err := h.ApiClient.Operations.GetNewsletterID(&newsParams)
	if err != nil {
		l.Errorf("Error getting newsletters from backend %v", err)
		ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
			"Title": "Error 500",
			"Error": "Communication server error",
		})
		return
	}

	resultNewsletter := make([]NewsLetter, 0, len(newsletters.Payload))
	for _, val := range newsletters.Payload {
		news := NewsLetter{
			ID:         val.ID,
			Start:      (*time.Time)(val.StartDatetime),
			Stop:       (*time.Time)(val.StopDatetime),
			FilterType: *val.FilterType,
			Filter:     val.Filter,
		}
		resultNewsletter = append(resultNewsletter, news)
	}
	ctx.HTML(http.StatusOK, "newsletter.html", gin.H{
		"Title":                "Newsletter list",
		"Newsletters":          resultNewsletter,
		"NewsletterCreatedTag": false,
	})
}

func (h *Handler) CreateNewsletter(ctx *gin.Context) {
	ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
		"Title": "Error 501",
		"Error": "Not implemented",
	})
}

func (h *Handler) ChangeNewsletter(ctx *gin.Context) {
	ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
		"Title": "Error 501",
		"Error": "Not implemented",
	})
}

func (h *Handler) DeleteNewsletter(ctx *gin.Context) {
	ctx.HTML(http.StatusInternalServerError, "error.html", gin.H{
		"Title": "Error 501",
		"Error": "Not implemented",
	})
}
