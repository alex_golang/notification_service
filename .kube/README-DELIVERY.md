# Deploy Delivery in the kubernetes

Hello before staring please check the precondition

### Precondition
The Postgresql 14 must be already been installed somewhere else or in k8s also.
The DB must have already created db with name "notifydb" and user has to has necessary rights (SELECT, INSERT, UPDATE and so on)


## Install ingress-nginx controller
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx
```

## Create secret for DB connection string and TOKEN string
need to replace all CAPITAL 

Create DB string secret
```
kubectl create secret generic apisec \
  --from-literal dsn="host=HOST port=PORT user=USER password=PASSWORD dbname=notifydb sslmode=require"
```

Create token secret
```
kubectl create secret generic deliverysec \
  --from-literal token="TOKEN"
```

## Apply the delivery.yaml
```
# kubectl apply -f delivery.yaml
deployment.apps/notification-delivery-dep created
service/notification-delivery-svc created
ingress.networking.k8s.io/notification-delivery-ing created
```

after some type the ingress svc will get external IP
```
# kubectl get svc 
NAME                                 TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)                         AGE
ingress-nginx-controller             LoadBalancer   10.245.86.32    143.244.199.70   80:31407/TCP,443:30107/TCP      24m
ingress-nginx-controller-admission   ClusterIP      10.245.95.30    <none>           443/TCP                         24m
kubernetes                           ClusterIP      10.245.0.1      <none>           443/TCP                         31m
notification-delivery-svc            LoadBalancer   10.245.118.82   161.35.247.90    7766:32576/TCP                  79s
```
our service name is `notification-delivery-svc` the external IP is 161.35.247.90

Prometheus metrics
```
curl -v http://161.35.247.90:7766/metrics
```

## Removing API
```
# kubectl delete  -f delivery.yaml
deployment.apps "notification-delivery-dep" deleted
service "notification-delivery-svc" deleted
ingress.networking.k8s.io "notification-delivery-ing" deleted
```