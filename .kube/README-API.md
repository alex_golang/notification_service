# Deploy API in the kubernetes

### Precondition
The Postgresql 14 must be already been installed somewhere else or in k8s also.
The DB must have already created db with name "notifydb" and user has to has necessary rights (SELECT, INSERT, UPDATE and so on)


## Install ingress-nginx controller
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx
```

## Create secret for DB connection string
need to replace all CAPITAL 

```
kubectl create secret generic apisec \
  --from-literal dsn="host=HOST port=PORT user=USER password=PASSWORD dbname=notifydb sslmode=require"
```

## Apply the api.yaml
```
# kubectl apply -f api.yaml
deployment.apps/notification-api-dep created
service/notification-api-svc created
ingress.networking.k8s.io/notification-api-ing created
```
after some type the ingress svc will get external IP
```
# kubectl get svc 
NAME                                 TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)                         AGE
ingress-nginx-controller             LoadBalancer   10.245.86.32    143.244.199.70   80:31407/TCP,443:30107/TCP      24m
ingress-nginx-controller-admission   ClusterIP      10.245.95.30    <none>           443/TCP                         24m
kubernetes                           ClusterIP      10.245.0.1      <none>           443/TCP                         31m
notification-api-svc                 LoadBalancer   10.245.118.82   161.35.247.76    8090:30375/TCP,7755:31306/TCP   9m29s
```
our service name is `notification-api-svc` the external IP is 161.35.247.76

API access
```
curl -v http://161.35.247.76:8090/tags
```
Prometheus metrics
```
curl -v http://161.35.247.76:7755/metrics
```

## Removing API
```
# kubectl delete -f api.yaml
deployment.apps "notification-api-dep" deleted
service "notification-api-svc" deleted
ingress.networking.k8s.io "notification-api-ing" deleted
```
