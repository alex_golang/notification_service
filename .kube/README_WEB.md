# Deploy WEB in the kubernetes

Hello before staring please check the precondition

### Precondition
Check the auth0 variable in the yaml file, you have to use your own data.
your callback address should be in the allowed list in the auth0 profile


## Install ingress-nginx controller
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx
```

## Create secret for DB connection string and TOKEN string
need to replace all CAPITAL 

Create auth0 secret 
```
kubectl create secret generic websec \
  --from-literal auth-client-secret="TOKEN HERE"
```  

## Apply the delivery.yaml
```
# kubectl apply -f web.yaml
deployment.apps/notification-web-dep created
service/notification-web-svc created
ingress.networking.k8s.io/notification-web-ing created
```

after some type the ingress svc will get external IP
```
# kubectl get svc
NAME                                 TYPE           CLUSTER-IP       EXTERNAL-IP       PORT(S)                         AGE
ingress-nginx-controller             LoadBalancer   10.245.215.118   159.223.242.224   80:31747/TCP,443:31113/TCP      17m
ingress-nginx-controller-admission   ClusterIP      10.245.142.111   <none>            443/TCP                         17m
kubernetes                           ClusterIP      10.245.0.1       <none>            443/TCP                         24m
notification-api-svc                 LoadBalancer   10.245.175.87    161.35.244.38     8090:30372/TCP,7755:32021/TCP   11m
notification-delivery-svc            LoadBalancer   10.245.24.109    161.35.247.248    7766:30527/TCP                  11m
notification-web-svc                 LoadBalancer   10.245.247.48    143.244.197.49    3030:32715/TCP                  4m11s
```
our service name is `notification-web-svc` the external IP is 143.244.197.49

open browser and copy-paste the address 143.244.197.49:3030 
and you have to see the main page of the web view

## Removing WEB
```
# kubectl delete  -f web.yaml
deployment.apps "notification-web-dep" deleted
service "notification-web-svc" deleted
ingress.networking.k8s.io "notification-web-ing" deleted
```