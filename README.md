# Сервис уведомлений
### Задача
Необходимо разработать сервис управления рассылками API администрирования и получения статистики.
Полные условия [тут](https://www.craft.me/s/n6OVYFVUpq0o6L)

## Описание реализации
Реализовано три сервиса backend - сам API, delivery - сервис общения со внешним API и controller - frontend для backend API
Сервис БД на основе СУБД PostgreSQL.
Delivery сервис работает в фоне и на основе таймера `DELIVERY_TIMER` проверяет БД на наличие новых рассылкой. Как только появляются новые клиента, таймер останавливается до обработки всего списка. Получив список клиентов, запускается пул из воркеров размером `DELIVERY_WORKERS` и происходит отправка сообщений клиентам. При каждом изменении статуса отправки, обновляется значение в соответствующей таблице для сообщения (клиента).
Сервис Backend предоставляет доступ к каждой таблице в БД на создание, просмотр, изменение, удаление записей, а так же вывод статистику по каждой рассылке.
Сервис controller это web view представляние API. Этот сервис реализовано частично, для реализации oauth, работает ручка /tags (получение списка тегов- GET и добавление тегов - POST).

## Реализованный основной функционал
- создание, просмотр, изменение, удаление сущностей "рассылка"
- создание, просмотр, изменение, удаление сущностей "клиент"
- получение статистики по отправленным сообщениям каждому клиенту по каждой рассылке
- обработки активных рассылок и отправки сообщений клиентам

### Реализованный дополнительный функционал
- подготовлен docker-compose для запуска всех сервисов проекта одной командой
- написаны конфигурационные файлы для деплоя в kubernetes
- по адресу /docs/ открывается страница со Swagger UI и в нём отображается описание разработанного API
- реализован (частично) администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям. 
- обеспечена интеграцию с внешним OAuth2 сервисом авторизации для административного интерфейса. Пример: https://auth0.com
- организована обработка ошибок и откладывание запросов при неуспехе для последующей повторной отправки.
- реализована отдача метрик в формате prometheus и задокументированы эндпоинты и экспортируемые метрики [описание метрик ниже](#Описание-метрик-Prometheus)
- подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в логах нужную информацию.

### Нереализованный дополнительный функционал
- тестирование написанного кода
- обеспечить автоматическую сборку/тестирование с помощью GitLab CI
- дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
- соблюдение timezone


## Как запустить проект

Предварительно нужно проверить `.env` файл переменных окружения

| Переменная окружения         | Описание |
|--------------|:-----|
| **Database related variables** |
|APP_DSN| Строка пподключения к БД PostgreSQL.  Пример "host=localhost port=5432 user=postgres password=postgres dbname=notifydb sslmode=disable" |
|APP_DBPOOLCOUNT | Количество одновременных подключений к БД. Пример. 50 |
|**API Variables**||
|API_DB_TIMEOUT | Timeout для запроса в БД для API сервиса |
|API_METRIC_HOST | IP адрес интерфейса на котором будут выдаваться API метрики, Пример `0.0.0.0` |
|API_METRIC_PORT | Порт на котором будут выдаваться метрики, Пример `7755` |
|API_LOGLEVEL | Уровень логирования. Пример. `debug` |
|API_HOST| IP адрес интерфейса на котором будет работать API, Пример `0.0.0.0` |
|API_PORT| Порт на котором будут выдаваться метрики, Пример  `8090` |
| **Delivery variables** |
|DELIVERY_DB_TIMEOUT | Timeout для запроса в БД для сервиса delivery |
|DELIVERY_METRIC_HOST| IP адрес интерфейса на котором будут выдаваться delivery метрики, Пример `0.0.0.0`|
|DELIVERY_METRIC_PORT| Порт на котором будут выдаваться метрики, Пример `7766`|
|DELIVERY_URL| Адрес внешнего сервиса Пример: `https://probe.fbrq.cloud/v1/send`|
|DELIVERY_TOKEN| Токен для внешнего сервиса|
|DELIVERY_WORKERS| Количество воркеров (потоков) для отправки сообщний. Пример `10`|
|DELIVERY_TIMER| Таймер для проверки новых клиентов в БД. Пример `60`|
|DELIVERY_TIMEOUT| Timeout для работы со внешним сервисов Пример `10`|
|DELIVERY_LOGLEVEL| Уровень логирования. Пример `debug` или `info` и пр.|
| **Auth0 variables** |
|AUTH0_DOMAIN| auth0 domain |
|AUTH0_CLIENT_ID| auth0 client id |
|AUTH0_CLIENT_SECRET| auth0 client secret |
|AUTH0_CALLBACK_URL| auth0 callback address |
| **Web controller variables** | |
|BACKEND_NAME| name of the backend container Example: backend|

#### Через docker-compose
Скачать репозиторий и перейти в созданную папку.
выполнить команду 
```
docker-compose up -d
```
API будет работать на указанных в .env файле портах.

#### В kubernetes
В папке `.kube` есть подробное описание по развертыванию API и Delivery сервиса в kubernetes.


# Описание метрик Prometheus
### Метрики сервиса API
| Метрика         | Описание |
|--------------|:-----|
| **Authentication metrics** |
| total_request_to_api |  Total number of requests |
| authentication_call      |  Total number of calling authentication function |
| authentication_call_success      |  counter of successfully authenticated calls |
| authentication_call_failed      |  counter of failed authenticated calls |
| **User related metrics** | |
| create_user_call      |  request for creating a user |
| get_users_call      |  request for getting users |
| get_user_call      |  request for getting a user |
| delete_user_call      |  requesr for deleting a user |
| change_user_call      |  request for changing a user |
| create_user_failed      |  failed request for creating a user |
| get_users_failed      |  failed request for getting users |
| get_user_failed      | failed request for creating a user  |
| delete_user_failed      | failed request for deleting a user  |
| changing_user_failed      |  failed request for changing a user |
| **newsletter related metrics**| |
| create_newsletter_call | request for creating a newsletter |
| get_newsletters_call | request for getting newsletters |
| get_newsletter_call | request for getting a newsletter |
| delete_newsletter_call | requesr for deleting a newsletter |
| change_newsletter_call | request for changing a newsletter |
| create_newsletter_failed | failed request for creating a newsletter |
| get_newsletters_failed | failed request for getting newsletters |
| get_newsletter_failed | failed request for creating a newsletter |
| delete_newsletter_failed | failed request for deleting a newsletter |
| changing_newsletter_failed | failed request for changin a newsletter |
| **tags related metrics**| |
| create_tag_call| request for creating a tag|
| get_tags_call| request for getting tags|
| create_tag_failed| failed request for creating a tag|
| get_tags_failed| failed request for getting tags|
|**operating time of each function**||
| user_request_duration_seconds |Duration of the request for all opearation with users |
| newsletter_request_duration_seconds| Duration of the request for all opearation with newsletters |

### Метрики сервиса Delivery
| Метрика         | Описание |
|--------------|:-----|
|**External API requests**||
| message_sent_total | counter for successfully sent messages |
| message_sent_error | counter for failed send messages |
| external_server_error | counter for external server error |
| external_server_success | counter for external server success |
| message_to_sent | number of message to sent, i.e. lenght of message queue |
