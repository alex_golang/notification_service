package psql

import (
	"context"
	"deliverier/internal/models"
	"fmt"

	"github.com/jackc/pgx"
)

// GetActiveSubsribers возвращает из БД всех клиентов кому надо отправить рассылку в текущий момент времени
func (i *Instance) GetActiveSubsribers(ctx context.Context) ([]*models.ActiveSubscribers, error) {
	rows, err := i.Conn.Query(ctx, "SELECT newsletter_id, start, stop, text, client_id, clients FROM active_subscribers_all;")
	if err == pgx.ErrNoRows {
		return nil, fmt.Errorf("Select returned empty: %v", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Select error: %v", err)
	}

	defer rows.Close()

	subs := make([]*models.ActiveSubscribers, 0)

	for rows.Next() {
		sub := new(models.ActiveSubscribers)
		err := rows.Scan(&sub.ID, &sub.Start, &sub.Stop, &sub.Text, &sub.ClientID, &sub.ClientPhone)
		if err != nil {
			return nil, fmt.Errorf("Select scan error: %v", err)
		}
		subs = append(subs, sub)
	}
	return subs, nil
}

// UpdateStatus updates current status in the message table
func (i *Instance) UpdateStatus(ctx context.Context, msg *models.Message) (int, error) {

	//f_iu_get_id (nid integer, cid integer, _status integer, updateTime timestamp)
	row, err := i.Conn.Query(ctx, "select f_iu_get_id($1,$2,$3,$4) as id;",
		msg.NewsletterID, msg.ClientID, msg.Status, msg.UpdateTime)
	if err != nil {
		return 0, fmt.Errorf("Insert into message news_id %d and client_id %d error: %v", msg.NewsletterID, msg.ClientID, err)
	}
	defer row.Close()
	var id int
	for row.Next() {
		err = row.Scan(&id)
		if err != nil {
			return 0, fmt.Errorf("Error parsing update message, error: %v", err)
		}
	}
	return id, nil
}
