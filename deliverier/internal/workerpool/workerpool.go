package workerpool

import (
	"fmt"
	"sync"

	"github.com/sirupsen/logrus"
)

type Pool interface {
	// Start gets the workerpool ready to process jobs, and should only be called once
	Start()
	// Stop stops the workerpool, tears down any required resources,
	// and should only be called once
	Stop()
	// AddWork adds a task for the worker pool to process. It is only valid after
	// Start() has been called and before Stop() has been called.
	AddWork(Task)
}

type Task interface {
	// Execute performs the work
	Execute() error
	// OnFailure handles any error returned from Execute()
	OnFailure(error)
}

type WorkerPool struct {
	numWorkers int
	tasks      chan Task

	// ensure the pool can only be started once
	start sync.Once
	// ensure the pool can only be stopped once
	stop sync.Once

	// close to signal the workers to stop working
	quit chan struct{}
	log  *logrus.Logger
}

var ErrNoWorkers = fmt.Errorf("attempting to create worker pool with less than 1 worker")
var ErrNegativeChannelSize = fmt.Errorf("attempting to create worker pool with a negative channel size")

func NewWorkerPool(numWorkers int, channelSize int, logger *logrus.Logger) (Pool, error) {
	if numWorkers <= 0 {
		return nil, ErrNoWorkers
	}
	if channelSize < 0 {
		return nil, ErrNegativeChannelSize
	}

	tasks := make(chan Task, channelSize)

	return &WorkerPool{
		numWorkers: numWorkers,
		tasks:      tasks,

		start: sync.Once{},
		stop:  sync.Once{},

		quit: make(chan struct{}),
		log:  logger,
	}, nil
}

func (p *WorkerPool) Start() {
	p.start.Do(func() {
		p.log.Info("starting worker pool")
		p.startWorkers()
	})
}

func (p *WorkerPool) Stop() {
	p.stop.Do(func() {
		p.log.Info("stopping worker pool")
		close(p.quit)
	})
}
func (p *WorkerPool) AddWork(t Task) {
	select {
	case p.tasks <- t:
	case <-p.quit:
	}
}
func (p *WorkerPool) AddWorkNonBlocking(t Task) {
	go p.AddWork(t)
}

func (p *WorkerPool) startWorkers() {
	for i := 0; i < p.numWorkers; i++ {
		go func(workerNum int) {
			p.log.Debugf("starting worker %d", workerNum)

			for {
				select {
				case <-p.quit:
					p.log.Debugf("stopping worker %d with quit channel", workerNum)
					return
				case task, ok := <-p.tasks:
					if !ok {
						p.log.Debugf("stopping worker %d with closed tasks channel", workerNum)
						return
					}

					if err := task.Execute(); err != nil {
						p.log.Errorf("Task execution failed, worker %d", workerNum)
						task.OnFailure(err)
					}
				}
			}
		}(i)
	}
}
