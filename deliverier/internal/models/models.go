package models

import (
	"encoding/json"
	"time"
)

// ActiveNewsletter ActiveNewsletter model
type ActiveNewsletter struct {
	//ID of newsletter
	ID *string `json:"newsletter_id"`
	//Start - datetime when the newsletter start to mailing
	Start *time.Time `json:"start"`
	//Stop - datetime when the newsletter start to mailing
	Stop *time.Time `json:"stop"`
	//Text - message in mailing
	Text *string `json:"text"`
	//Filter-Type is value mac, tag, both. Based on this filter the clients will be selected
	FilterType *string `json:"filter_type"`
}

// MarshalBinary interface implementation
func (m *ActiveNewsletter) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	b, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// UnmarshalBinary interface implementation
func (m *ActiveNewsletter) UnmarshalBinary(b []byte) error {
	var res ActiveNewsletter
	if err := json.Unmarshal(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// ActiveNewsletter ActiveNewsletter model
type ActiveSubscribers struct {
	//ID of newsletter
	ID int `json:"newsletter_id"`
	//Start - datetime when the newsletter start to mailing
	Start time.Time `json:"start"`
	//Stop - datetime when the newsletter start to mailing
	Stop time.Time `json:"stop"`
	//Text - message in mailing
	Text string `json:"text"`
	//Client ID - client's id
	ClientID int `json:"client_id"`
	//Client mobile phone
	ClientPhone string `json:"client_phone"`
}

// MarshalBinary interface implementation
func (m *ActiveSubscribers) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	b, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// UnmarshalBinary interface implementation
func (m *ActiveSubscribers) UnmarshalBinary(b []byte) error {
	var res ActiveSubscribers
	if err := json.Unmarshal(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// ActiveNewsletter ActiveNewsletter model
type Message struct {
	//Message id
	ID int `json:"id"`
	//ID of newsletter
	NewsletterID int `json:"newsletter_id"`
	//Client ID - client's id
	ClientID int `json:"client_id"`
	//UpdateTime - message in mailing
	UpdateTime time.Time `json:"update_time,omitempty"`
	//Status of message
	Status string `json:"client_phone,omitempty"`
}

// Данные из БД, таблица status.
const (
	SENDING string = "SENDING"
	SENT    string = "SENT"
	ERROR   string = "ERROR"
)

// MarshalBinary interface implementation
func (m *Message) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	b, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// UnmarshalBinary interface implementation
func (m *Message) UnmarshalBinary(b []byte) error {
	var res Message
	if err := json.Unmarshal(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// External service request
type ExternalMsgReq struct {
	ID    int    `json:"id"`
	Phone int    `json:"phone"`
	Text  string `json:"text"`
}

// MarshalBinary interface implementation
func (m *ExternalMsgReq) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	b, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// UnmarshalBinary interface implementation
func (m *ExternalMsgReq) UnmarshalBinary(b []byte) error {
	var res ExternalMsgReq
	if err := json.Unmarshal(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// External service response
type ExternalMsgRes struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// MarshalBinary interface implementation
func (m *ExternalMsgRes) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	b, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return b, nil
}

// UnmarshalBinary interface implementation
func (m *ExternalMsgRes) UnmarshalBinary(b []byte) error {
	var res ExternalMsgRes
	if err := json.Unmarshal(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
