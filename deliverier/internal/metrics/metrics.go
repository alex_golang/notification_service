package metrics

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

var MessageSent prometheus.Counter
var MessageError prometheus.Counter
var ExternalServerError prometheus.Counter
var ExternalServerSuccess prometheus.Counter
var MessageToSent prometheus.Gauge

type MetricServer struct {
	Host      string
	Port      string
	Path      string
	MetricLog *logrus.Logger
}

func NewMetricServer() *MetricServer {
	return &MetricServer{}
}

func (m *MetricServer) Start(ctx context.Context) {
	wg := new(sync.WaitGroup)
	srv := http.Server{Addr: fmt.Sprintf("%s:%s", m.Host, m.Port)}
	ml := m.MetricLog.WithFields(logrus.Fields{
		"metric":           "Delivery service metric server",
		"host:port params": fmt.Sprintf("%s:%s", m.Host, m.Port),
	})

	select {
	case <-ctx.Done():
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			ml.Fatalf("metric server error %v", err)
		}
		wg.Wait()
		return
	default:
		MessageSent = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "message_sent_total",
			Help: "counter for successfully sent messages",
		})
		MessageError = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "message_sent_error",
			Help: "counter for failed send messages",
		})
		ExternalServerError = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "external_server_error",
			Help: "counter for external server error",
		})
		ExternalServerSuccess = prometheus.NewCounter(prometheus.CounterOpts{
			Name: "external_server_success",
			Help: "counter for external server success",
		})
		MessageToSent = prometheus.NewGauge(prometheus.GaugeOpts{
			Name: "message_to_sent",
			Help: "number of message to sent, i.e. lenght of message queue",
		})
		prometheus.Register(MessageSent)
		prometheus.Register(MessageError)
		prometheus.Register(ExternalServerError)
		prometheus.Register(ExternalServerSuccess)
		prometheus.Register(MessageToSent)
		http.Handle(fmt.Sprintf("/%s", m.Path), promhttp.Handler())

		wg.Add(1)
		go func() {
			defer wg.Done()
			m.MetricLog.Infof("Metric server is listening %s", fmt.Sprintf("http://%s:%s/%s", m.Host, m.Port, m.Path))
			err := srv.ListenAndServe()
			if err != nil && err != http.ErrServerClosed {
				ml.Fatalf("metric server error %v", err)
			}
			if err == http.ErrServerClosed {
				m.MetricLog.Info("graceful shutdown metric server")
				return
			}
		}()
	}
}
