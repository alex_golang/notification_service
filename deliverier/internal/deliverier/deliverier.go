package deliverier

import (
	"bytes"
	"context"
	psql "deliverier/internal/database"
	"deliverier/internal/metrics"
	"deliverier/internal/models"
	"deliverier/internal/workerpool"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
)

type Deliverier struct {
	//Database connection
	DB psql.Database
	//Logging entity
	Log *logrus.Logger
	//URL for mailing
	URL string
	//Access Token for mailing service
	Token string
	ctx   context.Context
	//Timer- every timer expiry the service will check active subscribers. In seconds
	Timer int
	//Number of Workers
	Workers int
	//Timeout for external mailing service
	Timeout int
}

type task struct {
	user *models.ActiveSubscribers
	msg  *models.Message
}

var Delivery Deliverier

// GetActiveSubsribers возвращает всех клиентов кому надо отправить рассылку в текущий момент времени
func GetActiveSubscriber(ctx context.Context) ([]*models.ActiveSubscribers, error) {
	ctxDB, _ := context.WithTimeout(ctx, Delivery.DB.Timeout*time.Second)
	users, err := Delivery.DB.GetActiveSubsribers(ctxDB)
	if err != nil {
		return nil, err
	}
	return users, nil
}

// Update status for the message
func UpdateStatus(ctx context.Context, msg *models.Message) (int, error) {
	ctxDB, _ := context.WithTimeout(ctx, Delivery.DB.Timeout*time.Second)
	id, err := Delivery.DB.UpdateStatus(ctxDB, msg)
	if err != nil {
		return 0, err
	}
	return id, nil
}

// isStopTimeOK - current time should not be greater than stop time in the newsletter.
func isStopTimeOK(stop time.Time) bool {
	now := time.Now()
	if now.Before(stop) {
		return true
	}
	return false
}

// Method for sending the message via external service
func (d task) Execute() error {
	l := Delivery.Log.WithFields(logrus.Fields{
		"function":   "Execute",
		"cell_phone": d.user.ClientPhone,
	})
	var err error
	d.msg = &models.Message{
		NewsletterID: d.user.ID,
		ClientID:     d.user.ClientID,
		UpdateTime:   time.Now().Local(),
		Status:       models.SENDING,
	}
	ctx, _ := context.WithTimeout(context.Background(), Delivery.DB.Timeout*time.Second)
	d.msg.ID, err = UpdateStatus(ctx, d.msg)
	l.Debugf("Update the message id %d - sending", d.msg.ID)
	if err != nil {
		l.Errorf("error adding into the message table %v", err)
		return err
	}
	if !isStopTimeOK(d.user.Stop) {
		l.Info("stopTime already passed")
		return errors.New("stopTime already passed")
	}
	l.Info("Sending message to client")

	phoneNum, err := strconv.Atoi(d.user.ClientPhone)
	if err != nil {
		l.Errorf("Error converting phone number, phone %s", d.user.ClientPhone)
		return err
	}

	msg := models.ExternalMsgReq{
		ID:    d.msg.ID,
		Phone: phoneNum,
		Text:  d.user.Text,
	}
	data, err := msg.MarshalBinary()
	if err != nil {
		l.Errorf("Error converting data2byte message, error %v", err)
		return err
	}
	body, err := Requester(d.msg.ID, data)
	if err != nil {
		l.Errorf("Error sending request to external service, error %v", err)
		return err
	}
	m := models.ExternalMsgRes{}
	err = m.UnmarshalBinary(body)
	if err != nil {
		l.Errorf("client: error unmarshalling the body: %s", err)
		return err
	}
	l.WithFields(logrus.Fields{
		"code":    m.Code,
		"message": m.Message,
	}).Infof("message is sent")

	d.msg.UpdateTime = time.Now().Local()
	d.msg.Status = models.SENT
	metrics.MessageSent.Inc()
	metrics.MessageToSent.Dec()
	d.msg.ID, err = UpdateStatus(ctx, d.msg)
	l.Debugf("Update the message id %d - sent", d.msg.ID)
	if err != nil {
		l.Errorf("error updating the message table %v", err)
		return err
	}
	return nil
}

// OnFailure execute action when the message is not sent
func (d task) OnFailure(err error) {
	l := Delivery.Log.WithFields(logrus.Fields{
		"function":   "OnFailure",
		"cell_phone": d.user.ClientPhone,
	})
	l.Errorf("error %v", err)
	ctx, _ := context.WithTimeout(context.Background(), Delivery.DB.Timeout*time.Second)

	d.msg = &models.Message{
		NewsletterID: d.user.ID,
		ClientID:     d.user.ClientID,
		UpdateTime:   time.Now().Local(),
		Status:       models.ERROR,
	}
	metrics.MessageError.Inc()
	d.msg.ID, err = UpdateStatus(ctx, d.msg)
	l.Debugf("Update the message id %d - error", d.msg.ID)
	if err != nil {
		l.Errorf("error updating the message table %v", err)
	}
	return
}

// Actual serving function
func Serv(ctx context.Context) error {
	l := Delivery.Log.WithField("function", "Serv")
	users, err := GetActiveSubscriber(ctx)
	if err != nil {
		l.Errorf("cannot get active subscribers %v", err)
		return err
	}

	metrics.MessageToSent.Set(float64(len(users)))
	l.Infof("active users: %d", len(users))
	if users != nil && len(users) <= 0 {
		return nil
	}

	//Initialization workerPool
	p, _ := workerpool.NewWorkerPool(Delivery.Workers, 0, Delivery.Log)
	p.Start()
	var t task
	for _, user := range users {
		t.user = user
		p.AddWork(t)
	}
	p.Stop()

	return nil
}

// Every Delivery.Timer it checks new subscribers.
func Start(ctx context.Context) {
	l := Delivery.Log.WithField("function", "Start")
	l.Info("Start of working")
	for {
		select {
		case <-ctx.Done():
			l.Info("shutdown the mailing server")
			return
		default:
			l.Debug("Check the DB for the new subscribers")
			err := Serv(ctx)
			if err != nil {
				l.Errorf("error %+v", err)
			}
		}
		l.Debugf("Sleep %d seconds", Delivery.Timer)
		time.Sleep(time.Duration(Delivery.Timer) * time.Second)
	}
}

// The function makes request to an external service for sending notification
func Requester(id int, data []byte) ([]byte, error) {
	l := Delivery.Log.WithField("function", "Requester")
	sendBuf := bytes.NewBuffer(data)
	URL := fmt.Sprintf("%s/%d", Delivery.URL, id)
	l.Debugf("URL %s", URL)
	l.Debugf("request body: %+v", string(data))
	req, err := http.NewRequest(http.MethodPost, URL, sendBuf)

	if err != nil {
		return nil, fmt.Errorf("client: could not create request: %s", err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", Delivery.Token))
	req.Header.Set("accept", "application/json")

	client := http.Client{
		Timeout: time.Duration(Delivery.Timeout) * time.Second,
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("client: error making http request: %s", err)
	}
	defer res.Body.Close()
	if res.Status != "200 OK" {
		metrics.ExternalServerError.Inc()
		return nil, fmt.Errorf("client: remote server reply: %s", res.Status)
	}
	metrics.ExternalServerSuccess.Inc()
	l.Debugf("respone headers: %+v", res.Header)
	body, _ := io.ReadAll(res.Body)
	l.Debugf("response Body: %+v", string(body))
	return body, nil
}
